import 'package:flutter/material.dart';
import 'package:tabanan/src/screens/authentication/screen.dart';
import 'package:tabanan/src/routes/authentication_route.dart';

class App extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: 'Trans Serasi',
			theme: ThemeData(
				primarySwatch: Colors.red,
				buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
				textTheme: Typography.tall2018,
			),
			home: Screen(),
			debugShowCheckedModeBanner: false,
		);
	}
	
}

class Screen extends StatefulWidget {
	@override
	State createState() => ScreenState();
}

class ScreenState extends State<Screen> {
	
	@override
	void initState() {
		super.initState();
		Future
			.delayed(Duration(milliseconds: 100))
			.then((_) => Navigator.of(context).pushReplacement(AuthenticationRoute.build()));
	}
	
	@override
	Widget build(BuildContext context) {
		return Scaffold();
	}
}