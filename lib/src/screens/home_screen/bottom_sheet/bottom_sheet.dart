import 'package:flutter/material.dart';
import 'package:tabanan/src/routes/edit_route.dart';
import 'package:tabanan/src/routes/login_route.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:tabanan/src/components/logo_circle_view.dart';

class HomeBottomSheet extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Container(
			color: Color(0xFF737373),
			
			child: Container(
				
				decoration: BoxDecoration(
					color: Colors.white,
					borderRadius: BorderRadius.only(
						topLeft: Radius.circular(10),
						topRight: Radius.circular(10)
					)
				),
				
				child: Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					mainAxisSize: MainAxisSize.min,
					mainAxisAlignment: MainAxisAlignment.start,
					children: <Widget>[
						
						Container(
							height: 200,
							child: Container(
								padding: EdgeInsets.all(20),
								child: LogoCircleView()
							),
						),
						
						Divider(),
						
						ListTile(
							leading: Icon(Icons.person),
							title: Text("Akun"),
							onTap: () => Navigator.push(context, EditRoute.build())
						),
						
						ListTile(
							leading: Icon(Icons.power_settings_new),
							title: Text("Keluar"),
							onTap: () => FlutterSecureStorage()..deleteAll().then((_) => Navigator.pushAndRemoveUntil(context, LoginRoute.build(), (route) => route.isFirst)),
						)
					
					],
				),
			),
		);
	}
	
}