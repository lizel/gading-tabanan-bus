
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/rxdart.dart';

import '../models/index.dart';
import '../repositories/index.dart';

import '../../bus/models/index.dart';
import '../../bus/repositories/index.dart';

class DriverBloc {
	final _driverRepository = DriverRepository();
	final _busRepositry = BusRepository();
	
	final _busDetailChannel = BehaviorSubject<BusMovementContent>();
	final _driverChannel = BehaviorSubject<DriverModel>();
	final _logoutChannel = BehaviorSubject<Null>();
	
	final _startDateChannel = BehaviorSubject<DateTime>();
	final _startDateAssignmentChannel = BehaviorSubject<String>();
	final _startDateQueryChannel = BehaviorSubject<DateTime>();
	final _endDateChannel = BehaviorSubject<DateTime>();
	final _endDateAssignmentChannel = BehaviorSubject<String>();
	final _endDateQueryChannel = BehaviorSubject<DateTime>();
	final _formatter = DateFormat("dd-MM-yy HH:mm");
	final String platNo;
	
	DriverBloc({ this.platNo }) {
		_startDateQueryChannel.listen((input) => _evaluateTrack(start: input, end: _endDateQueryChannel.value));
		_endDateQueryChannel.listen((input) => _evaluateTrack(start: _startDateQueryChannel.value, end: input));
		
		_startDateChannel.listen((input) {
			_startDateQueryChannel.add(input);
			_startDateAssignmentChannel.add(_formatter.format(input));
		});
		
		_endDateChannel.listen((input) {
			_endDateQueryChannel.add(input);
			_endDateAssignmentChannel.add(_formatter.format(input));
		});
	}
	
	Stream<BusMovementContent> get busDetailChannel => _busDetailChannel.stream;
	Stream<DriverModel> get driverChannel => _driverChannel.stream;
	Stream<Null> get logoutChannel => _logoutChannel.stream;
	
	Stream<String> get startDateAssignmentChannel => _startDateAssignmentChannel.stream;
	Stream<String> get endDateAssignmentChannel => _endDateAssignmentChannel.stream;
	
	setStartDate(DateTime date, TimeOfDay time) {
		if (date != null && time != null) {
			_startDateChannel.add(date.add(Duration(hours: time.hour, minutes: time.minute)));
		}
	}
	
	setEndDate(DateTime date, TimeOfDay time) {
		if (date != null && time != null) {
			_endDateChannel.add(date.add(Duration(hours: time.hour, minutes: time.minute)));
		}
	}
	
	_evaluateTrack ({ DateTime start, DateTime end }) {
		print(start);
		if (start == null || end == null) { return; }
		if (start.compareTo(end) > 0) { return; }
		track(start: start, end: end);
	}
	
	Future<Null> track({ DateTime start, DateTime end }) async {
		final result = await _busRepositry.track(plat: platNo, start: start, end: end);
		if (result.length == 0) {
			return _busDetailChannel.addError(null);
		}
		final movement = result[0];
		movement.log_positions.sort((a, b) => a.no - b.no);
		movement.log_positions = movement.log_positions.take(100).toList();
		_busDetailChannel.add(result[0]);
	}
	
	Future<Null> detail() async {
		final result = await _driverRepository.detail(platNo);
		if (result.message.length == 0) {
			return _driverChannel.addError(null);
		}
		_driverChannel.add(result);
	}

	dispose() {
		_busDetailChannel.close();
		_driverChannel.close();
		_logoutChannel.close();
		_startDateChannel.close();
		_startDateAssignmentChannel.close();
		_startDateQueryChannel.close();
		_endDateChannel.close();
		_endDateAssignmentChannel.close();
		_endDateQueryChannel.close();
	}
}

