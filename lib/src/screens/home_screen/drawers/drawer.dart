import 'package:flutter/material.dart';

import 'drawer_head.dart';
import 'drawer_body.dart';

class HomeDrawer extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Drawer(
			child:LayoutBuilder(
				builder: (BuildContext context, BoxConstraints viewportConstraints) {
					return ConstrainedBox(
						constraints: BoxConstraints(maxHeight: viewportConstraints.maxHeight),
						child: Column(
							crossAxisAlignment: CrossAxisAlignment.stretch,
							mainAxisSize: MainAxisSize.max,
							mainAxisAlignment: MainAxisAlignment.start,
							children: <Widget>[
								SizedBox(height: 20),
								
								DrawerHead(),
								
								Divider(),
								
								Flexible(child: DrawerBody())
							]
						),
					);
				}
			)
		
		);
	}
}