import 'package:json_annotation/json_annotation.dart';

part 'school_model.g.dart';

@JsonSerializable()
class SchoolModel {
	int value;
	List<SchoolContent> message;
	
	SchoolModel(this.value, this.message);
	
	factory SchoolModel.fromJson(Map<String, dynamic> json) => _$SchoolModelFromJson(json);
	Map<String, dynamic> toJson() => _$SchoolModelToJson(this);
}

@JsonSerializable()
class SchoolContent {
	String KodeSekolah;
	String NamaSekolah;
	String TelpSekolah;
	String AlamatSekolah;
	String Tipe;
	String id;
	
	SchoolContent(this.KodeSekolah, this.NamaSekolah, this.TelpSekolah, this.AlamatSekolah, this.Tipe, this.id);
	
	factory SchoolContent.fromJson(Map<String, dynamic> json) => _$SchoolContentFromJson(json);
	Map<String, dynamic> toJson() => _$SchoolContentToJson(this);
}