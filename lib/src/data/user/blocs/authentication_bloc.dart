import 'dart:convert';
import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:tabanan/src/data/user/models/index.dart';
import 'package:tabanan/src/data/common/models/string_model.dart';
import '../forms/login_form.dart';
import '../validators/user_validator.dart';
import '../repositories/user_repository.dart';

export 'package:tabanan/src/data/common/models/string_model.dart';

class AuthenticationBloc with UserValidator {
	final storage = new FlutterSecureStorage();
	final _check = BehaviorSubject<bool>();
	final _dummy = BehaviorSubject<String>();
	
	Stream<bool> get check => _check.stream;
	Stream<String> get dummy => _dummy.stream;
	
	Future authenticate() async {
		final undecoded = await storage.read(key: "credential");
		if (undecoded == null) {
			return _check.sink.add(false);
		}
		_check.sink.add(true);
	}
	
	Function(String) get triggerDummy => _dummy.add;
	
	dispose() {
		_check.close();
		_dummy.close();
	}
}

