import 'package:flutter/material.dart';
import 'bottom_sheet/bottom_sheet.dart';
import 'package:tabanan/src/data/bus/blocs/home_bloc.dart';
import 'package:tabanan/src/routes/school_search_route.dart';

class SearchBar extends StatefulWidget {
	final HomeBloc bloc;
	SearchBar(this.bloc);
	@override
  	State<StatefulWidget> createState() => SearchBarState(bloc);
}

class SearchBarState extends State<SearchBar> {
	final HomeBloc bloc;
	
	SearchBarState(this.bloc);
	
	@override
	Widget build(BuildContext context) {
		return Container(
			
			child: Container (
				
				padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
				height: 90,
				child: Container (
					
					padding: EdgeInsets.all(10),
					decoration: BoxDecoration(
						color: Colors.white,
						borderRadius: BorderRadius.circular(9),
						boxShadow: [
							BoxShadow(
								color: Colors.black54,
								blurRadius: 3.0,
								spreadRadius: 0.0,
								offset: Offset(0, 1)
							),
						],
					),
					child: Row (
						crossAxisAlignment: CrossAxisAlignment.center,
						mainAxisAlignment: MainAxisAlignment.start,
						mainAxisSize: MainAxisSize.max,
						children: <Widget>[
							Flexible(
								child: Row(
									children: <Widget>[
										
										Icon(Icons.search, color: Colors.grey[600],),
										
										SizedBox(width: 10),
										
										Flexible(
											child: StreamBuilder<String>(
												stream: bloc.schoolName,
												builder: (context, snapshot) {
													return InkWell(
														onTap: () => Navigator.push(context, SchoolSearchRoute.build()).then(bloc.setSchool),
														child: Text(snapshot.data ?? "Cari Sekolah"),
													);
												}
											),
										)
									
									],
								),
							),
							
							SizedBox(width: 10),
							
							Container(
								child: ClipOval(
									child: Material(
										color: Colors.transparent,
										child: Ink.image(
											image: AssetImage('assets/images/app/logo.png'),
											width: 30.0,
											height: 30.0,
											fit: BoxFit.contain,
											child: InkWell(
												onTap: () => showModalBottomSheet(context: context, builder: (_) => HomeBottomSheet()),
											)
										),
									),
								),
							),
						
						],
					),
				)
			),
		);
	}
}