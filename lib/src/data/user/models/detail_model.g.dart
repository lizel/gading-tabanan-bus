// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DetailModel _$DetailModelFromJson(Map<String, dynamic> json) {
  return DetailModel(
      json['value'] as int,
      json['message'] == null
          ? null
          : DetailContent.fromJson(json['message'] as Map<String, dynamic>));
}

Map<String, dynamic> _$DetailModelToJson(DetailModel instance) =>
    <String, dynamic>{'value': instance.value, 'message': instance.message};

DetailContent _$DetailContentFromJson(Map<String, dynamic> json) {
  return DetailContent(json['email'] as String, json['KodeSekolah'] as String,
      json['NamaSekolah'] as String);
}

Map<String, dynamic> _$DetailContentToJson(DetailContent instance) =>
    <String, dynamic>{
      'email': instance.email,
      'KodeSekolah': instance.KodeSekolah,
      'NamaSekolah': instance.NamaSekolah
    };
