import 'package:json_annotation/json_annotation.dart';
part 'string_model.g.dart';

@JsonSerializable()
class StringModel {
	int value;
	String message;
	
	StringModel({ this.value, this.message });
	
	factory StringModel.fromJson(Map<String, dynamic> json) => _$StringModelFromJson(json);
	Map<String, dynamic> toJson() => _$StringModelToJson(this);
}