import 'package:meta/meta.dart';
import 'package:json_annotation/json_annotation.dart';

part 'register_form.g.dart';

@JsonSerializable()
class RegisterForm {
	String username;
	String email;
	String password;
	
	RegisterForm(this.username, this.email, this.password);
	
	factory RegisterForm.fromJson(Map<String, dynamic> json) => _$RegisterFormFromJson(json);
	Map<String, dynamic> toJson() => _$RegisterFormToJson(this);
}