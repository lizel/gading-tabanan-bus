import 'package:flutter/material.dart';
import 'package:tabanan/src/routes/edit_route.dart';
import 'package:tabanan/src/routes/login_route.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class DrawerBody extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Column(
			crossAxisAlignment: CrossAxisAlignment.stretch,
			mainAxisSize: MainAxisSize.max,
			mainAxisAlignment: MainAxisAlignment.start,
			children: <Widget>[
				
				ListTile(
					dense: true,
					leading: Icon(Icons.person),
					title: Text("Akun"),
					onTap: () => Navigator.push(context, EditRoute.build())
				),
				
				Flexible(flex: 1, child: Container()),
				
				Divider(),
				
				ListTile(
					dense: true,
					leading: Icon(Icons.power_settings_new),
					title: Text("Keluar"),
					onTap: () => FlutterSecureStorage()..deleteAll().then((_) => Navigator.pushAndRemoveUntil(context, LoginRoute.build(), (route) => route.isFirst)),
				)
			
			],
		);
	}
}