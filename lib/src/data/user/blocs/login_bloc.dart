import 'dart:convert';
import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:tabanan/src/data/user/models/index.dart';
import 'package:tabanan/src/data/common/models/string_model.dart';
import '../forms/login_form.dart';
import '../validators/user_validator.dart';
import '../repositories/user_repository.dart';

export 'package:tabanan/src/data/common/models/string_model.dart';

class LoginBloc with UserValidator {
	final repository = UserRepository();
	final storage = new FlutterSecureStorage();
	
	final _username = BehaviorSubject<String>();
	final _password = BehaviorSubject<String>();
	final _failedSubmission = BehaviorSubject<StringModel>();
	final _successSubmission = BehaviorSubject<CredentialModel>();
	final _resetSubmission = BehaviorSubject<StringModel>();
	
	Function(String) get setUsername => _username.sink.add;
	Stream<String> get username => _username.stream.transform(usernameValidatorTransformer);
	
	Function(String) get setPassword => _password.sink.add;
	Stream<String> get password => _password.stream.transform(passwordValidatorTransformer);
	
	Stream<bool> get validForm => Observable.combineLatest2<String, String, bool>(username, password, (u, p) { return true; });
	Stream<StringModel> get failedSubmission => _failedSubmission.stream;
	Stream<CredentialModel> get successSubmission => _successSubmission.stream;
	Stream<StringModel> get resetSubmission => _resetSubmission.stream;
	
	Future<Null> resetNotAvailable() async {
		_resetSubmission.add(StringModel(
			value: 0,
			message: "Silahkan isi User ID anda."
		));
	}
	
	Future<Null> reset() async {
		final result = await repository.reset(username: _username.value);
		_resetSubmission.add(result);
	}
	
	Future<Null> submit() async {
		final form = LoginForm(
			username: _username.value,
			password: _password.value
		);
		final result = await repository.login(form: form);
		
		if (result.value == 0) {
			return _failedSubmission.sink.add(result);
		}
		
		final detail = await repository.detail(username: form.username);
		final credential = CredentialModel.fromJson({}..addAll(detail.message.toJson())..addAll(form.toJson()));
		storage.write(key: "credential", value: json.encode(credential.toJson()));
		_successSubmission.sink.add(credential);
	}
	
	dispose() {
		_username.close();
		_password.close();
		_failedSubmission.close();
		_successSubmission.close();
		_resetSubmission.close();
	}
}

