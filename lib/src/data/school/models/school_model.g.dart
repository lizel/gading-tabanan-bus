// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'school_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SchoolModel _$SchoolModelFromJson(Map<String, dynamic> json) {
  return SchoolModel(
      json['value'] as int,
      (json['message'] as List)
          ?.map((e) => e == null
              ? null
              : SchoolContent.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$SchoolModelToJson(SchoolModel instance) =>
    <String, dynamic>{'value': instance.value, 'message': instance.message};

SchoolContent _$SchoolContentFromJson(Map<String, dynamic> json) {
  return SchoolContent(
      json['KodeSekolah'] as String,
      json['NamaSekolah'] as String,
      json['TelpSekolah'] as String,
      json['AlamatSekolah'] as String,
      json['Tipe'] as String,
      json['id'] as String);
}

Map<String, dynamic> _$SchoolContentToJson(SchoolContent instance) =>
    <String, dynamic>{
      'KodeSekolah': instance.KodeSekolah,
      'NamaSekolah': instance.NamaSekolah,
      'TelpSekolah': instance.TelpSekolah,
      'AlamatSekolah': instance.AlamatSekolah,
      'Tipe': instance.Tipe,
      'id': instance.id
    };
