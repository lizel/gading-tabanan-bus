import 'dart:convert';
import 'package:http/http.dart' show Client;

import 'package:tabanan/src/data/config/configuration_bloc.dart';

import '../common/bus_provider_tiers.dart';
import '../models/index.dart';

class BusApiProvider implements BusSource {
	final _root = configurationBloc.endpoint.url;
	final _domain = configurationBloc.endpoint.domain;
	
	final _foxRoot = configurationBloc.foxlogger.url;
	final _foxKey = configurationBloc.foxlogger.key;
	
	final client = new Client();
	
	Future<List<BusContent>> list () async {
		final payload = { "key": _foxKey };
		final response = await client.get(Uri.https(_foxRoot, "/dataGPS", payload));
		final jsonResponse = json.decode(response.body) as List<dynamic>;
		return jsonResponse.cast<Map<String, dynamic>>().map((val) => BusContent.fromJson(val)).toList();
	}
	
	Future<List<BusContent>> fetch ({ String plat }) async {
		final payload = { "key": _foxKey, "license_plate_no": plat };
		final response = await client.get(Uri.https(_foxRoot, "/dataGPS", payload));
		final jsonResponse = json.decode(response.body) as List<dynamic>;
		return jsonResponse.map((val) => BusContent.fromJson(val));
	}
	
	Future<List<BusMovementContent>> track ({ String plat, String start, String end }) async {
		final payload = { "key": _foxKey, "license_plate_no": plat };
		if (start != null) { payload["date_start"] = start; }
		if (end != null) { payload["date_end"] = end; }
		final response = await client.get(Uri.https(_foxRoot, "/dataGPS_detail", payload));
		final jsonResponse = json.decode(response.body) as List<dynamic>;
		return jsonResponse.map((val) => BusMovementContent.fromJson(val)).toList();
	}
	
}