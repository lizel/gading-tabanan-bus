import 'package:flutter/material.dart';
import 'package:tabanan/src/routes/home_route.dart';
import 'package:tabanan/src/data/user/blocs/login_bloc.dart';
import 'package:tabanan/src/components/common_dialog.dart';
import 'package:tabanan/src/data/user/models/index.dart';

class LoginForm extends StatefulWidget {
	@override
	State<StatefulWidget> createState() => LoginFormState();
}

class LoginFormState extends State<LoginForm> {
	final bloc = LoginBloc();
	
	@override
	void initState() {
		super.initState();
		bloc.failedSubmission.listen(_failedSubmissionDialog);
		bloc.successSubmission.listen(_successSubmissionDialog);
		bloc.resetSubmission.listen(_resetSubmissionDialog);
	}
	
	_submit() {
		CommonDialog.showLoadingDialog(context);
		bloc.submit().whenComplete(() => Navigator.pop(context));
	}
	
	_successSubmissionDialog(CredentialModel credential) {
		Navigator.pushReplacement(context, HomeRoute.build());
	}
	
	_resetSubmissionDialog(StringModel snapshot) {
		return CommonDialog.showAlert(context, (context) {
			return AlertDialog(
				title: Text("Registrasi"),
				content: Column(
					mainAxisSize: MainAxisSize.min,
					crossAxisAlignment: CrossAxisAlignment.stretch,
					children: <Widget>[
						
						Text(snapshot.message),
						
						SizedBox(height: 20),
						
						SizedBox(
							height: 40,
							child: RaisedButton(
								onPressed: () => Navigator.pop(context),
								child: Text("Ok"),
							),
						)
					
					],
				),
			);
		});
	}
	
	_failedSubmissionDialog(StringModel snapshot) {
		CommonDialog.showAlert(context, (context) {
			return AlertDialog(
				title: Text("Login"),
				content: Column(
					mainAxisSize: MainAxisSize.min,
					crossAxisAlignment: CrossAxisAlignment.stretch,
					children: <Widget>[
						
						Text(snapshot.message),
						SizedBox(height: 20),
						SizedBox(
							height: 40,
							child: RaisedButton(
								onPressed: () => Navigator.pop(context),
								child: Text("Ok"),
							),
						)
					
					],
				),
			);
		});
	}
	
	@override
	Widget build(BuildContext context) {
		return Column(
			crossAxisAlignment: CrossAxisAlignment.stretch,
			mainAxisAlignment: MainAxisAlignment.start,
			mainAxisSize: MainAxisSize.min,
			children: <Widget>[
				
				StreamBuilder(
					stream: bloc.username,
					builder: (context, snapshot) {
						return TextField(
							onChanged: bloc.setUsername,
							decoration: InputDecoration(
								labelText: "User ID",
								hintText: "Input User ID terdaftar",
								hintStyle: Theme.of(context).textTheme.caption.copyWith(),
								suffixIcon: Icon(IconData(0xf345, fontFamily: "ionicons")),
								errorText: snapshot.error,
							),
						);
					},
				),
				
				StreamBuilder(
					stream: bloc.password,
					builder: (context, snapshot) {
						return TextField(
							onChanged: bloc.setPassword,
							decoration: InputDecoration(
								labelText: "Kata Sandi",
								hintText: "Input Kata Sandi.",
								hintStyle: Theme.of(context).textTheme.caption.copyWith(),
								suffixIcon: Icon(IconData(0xf1b0, fontFamily: "ionicons")),
								errorText: snapshot.error,
							),
							obscureText: true,
						);
					}
				),
				
				SizedBox(
					child: Column(
						crossAxisAlignment: CrossAxisAlignment.end,
						mainAxisAlignment: MainAxisAlignment.center,
						children: <Widget>[
							
							StreamBuilder(
								stream: bloc.username,
								builder: (context, snapshot) {
									return InkWell(
										onTap: snapshot.hasData? bloc.reset: bloc.resetNotAvailable,
										child: Text("Lupa Kata Sandi?", style: TextStyle(color: Theme.of(context).colorScheme.primary, fontWeight: FontWeight.bold)),
									);
								}
							),
							
						],
					),
					height: 50,
				),
				
				SizedBox(
					child: StreamBuilder(
						stream: bloc.validForm,
						builder: (context, snapshot) {
							return RaisedButton(
								onPressed: snapshot.hasData? _submit: null,
								child: Text("MASUK"),
								shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
							);
						}
					),
					width: double.infinity,
					height: 40,
				)
			
			],
		);
	}
	
	@override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }
}


