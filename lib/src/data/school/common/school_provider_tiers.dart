import 'package:tabanan/src/data/common/models/string_model.dart';
import 'package:tabanan/src/data/user/models/index.dart';
import '../models/index.dart';

abstract class SchoolSource {
	Future<SchoolModel> list();
}