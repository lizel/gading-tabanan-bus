import 'dart:async';
import 'dart:convert';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:tabanan/src/data/common/models/string_model.dart';
import '../forms/register_form.dart';
import '../models/index.dart';
import '../validators/user_validator.dart';
import '../repositories/user_repository.dart';


class EditBloc with UserValidator {
	final repository = UserRepository();
	
	final _email = BehaviorSubject<String>();
	final _password = BehaviorSubject<String>();
	final _repeatPassword = BehaviorSubject<String>();
	
	final _credentialChannel = BehaviorSubject<CredentialModel>();
	final _logoutChannel = BehaviorSubject<Null>();
	final _submission = BehaviorSubject<StringModel>();
	
	Function(String) get setEmail => _email.sink.add;
	Stream<String> get email => _email.stream.transform(emailValidatorTransformer);
	
	Function(String) get setPasswordRepeat => _repeatPassword.sink.add;
	Stream<String> get repeatPassword => _repeatPassword
		.withLatestFrom<String, Map<String, String>>(_password.stream, (p, rp) { return { "password": p, "repeat": rp }; })
		.transform(repeatPasswordValidatorTransformer);
	
	Function(String) get setPassword => _password.sink.add;
	Stream<String> get password => _password.stream.transform(passwordValidatorTransformer);
	
	Stream<CredentialModel> get credentialChannel => _credentialChannel.stream;
	Stream<bool> get validForm => Observable.combineLatest2<String, String, bool>(email, repeatPassword, (e, p) { return true; });
	Stream<Null> get logoutChannel => _logoutChannel.stream;
	Stream<StringModel> get submission => _submission.stream;
	
	Future<Null> fetch() async {
		final storage = new FlutterSecureStorage();
		final undecoded = await storage.read(key: "credential");
		if (undecoded == null) { return _logoutChannel.add(null); }
		
		var credential = CredentialModel.fromJson(json.decode(undecoded));
		final detail = await repository.detail(username: credential.username);
		
		credential = CredentialModel.fromJson({ "username": credential.username }..addAll(detail.message.toJson()));
		storage.write(key: "credential", value: json.encode(credential.toJson()));
		_credentialChannel.add(credential);
	}
	
	Future<Null> submit() async {
		final result = await repository.edit(form: RegisterForm (
			_credentialChannel.value.username,
			_email.value,
			_password.value,
		));
		_submission.sink.add(result);
	}
	
	dispose() {
		_email.close();
		_password.close();
		_repeatPassword.close();
		_logoutChannel.close();
		_submission.close();
		_credentialChannel.close();
	}
}

//
//class RegisterProvider extends InheritedWidget {
//	final RegisterBloc bloc;
//
//	RegisterProvider({ Key key, Widget child, this.bloc })
//		: super(key: key, child: child);
//
//	@override
//	bool updateShouldNotify(RegisterProvider oldWidget) => oldWidget.bloc != this.bloc;
//
//	static RegisterProvider of(BuildContext context) => context.inheritFromWidgetOfExactType(RegisterProvider);
//}