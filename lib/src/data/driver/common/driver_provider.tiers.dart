import 'package:tabanan/src/data/common/models/string_model.dart';
import '../models/index.dart';

abstract class DriverSource {
	Future<DriverModel> detail(String plat);
}