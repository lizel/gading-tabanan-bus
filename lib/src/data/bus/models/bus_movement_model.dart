import 'package:json_annotation/json_annotation.dart';

part 'bus_movement_model.g.dart';

@JsonSerializable()
class BusMovementModel {
	List<BusMovementContent> message;
	
	BusMovementModel(this.message);
	
	factory BusMovementModel.fromJson(Map<String, dynamic> json) => _$BusMovementModelFromJson(json);
	Map<String, dynamic> toJson() => _$BusMovementModelToJson(this);
}

@JsonSerializable()
class BusMovementContent {
	int vehicle_id;
	String license_plate_no;
	int qty_positions;
	double total_distance;
	List<BusMovementLog> log_positions;
	String merk;
	String type;
	String halo;
	String company;
	
	
	BusMovementContent(this.vehicle_id, this.license_plate_no,
		this.qty_positions, this.total_distance, this.log_positions, this.merk,
		this.type, this.halo, this.company);
	
	factory BusMovementContent.fromJson(Map<String, dynamic> json) => _$BusMovementContentFromJson(json);
	Map<String, dynamic> toJson() => _$BusMovementContentToJson(this);
}

@JsonSerializable()
class BusMovementLog {
	int no;
	double lat;
	double lng;
	DateTime date_time;
	bool status_engine;
	
	BusMovementLog(this.no, this.lat, this.lng, this.date_time, this.status_engine);
	
	factory BusMovementLog.fromJson(Map<String, dynamic> json) {
		return BusMovementLog(
			json['no'] as int,
			double.parse(json['lat']),
			double.parse(json['lng']),
			json['date_time'] == null
				? null
				: DateTime.parse(json['date_time'] as String),
			json['status_engine'] == "true");
	}
	
	Map<String, dynamic> toJson() => _$BusMovementLogToJson(this);
}