import 'package:json_annotation/json_annotation.dart';

part 'detail_model.g.dart';

@JsonSerializable()
class DetailModel {
	int value;
	DetailContent message;
	
	DetailModel(this.value, this.message);
	
	factory DetailModel.fromJson(Map<String, dynamic> json) => _$DetailModelFromJson(json);
	Map<String, dynamic> toJson() => _$DetailModelToJson(this);
}

@JsonSerializable()
class DetailContent {
	String email;
	String KodeSekolah;
	String NamaSekolah;
	
	DetailContent(this.email, this.KodeSekolah, this.NamaSekolah);
	
	factory DetailContent.fromJson(Map<String, dynamic> json) => _$DetailContentFromJson(json);
	Map<String, dynamic> toJson() => _$DetailContentToJson(this);
}