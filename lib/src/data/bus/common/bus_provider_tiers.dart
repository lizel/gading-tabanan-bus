import 'package:tabanan/src/data/common/models/string_model.dart';
import 'package:tabanan/src/data/user/models/index.dart';
import '../models/index.dart';

abstract class BusSource {
	Future<List<BusContent>> list();
	Future<List<BusContent>> fetch({ String plat });
	Future<List<BusMovementContent>> track ({ String plat, String start, String end });
}