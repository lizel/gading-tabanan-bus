import 'package:flutter/material.dart';

class StateDataWidget<T> extends InheritedWidget {
	final T state;
	
	StateDataWidget({ Key key, Widget child, this.state}):
			super(key: key, child: child);
	
	@override
	bool updateShouldNotify(StateDataWidget<T> oldWidget) => this.state != oldWidget.state;
	
	static StateDataWidget<T> of <T> (BuildContext context) {
		return context.inheritFromWidgetOfExactType(StateDataWidget<T>().runtimeType) as StateDataWidget<T>;
	}
}