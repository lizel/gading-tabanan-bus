import 'dart:async';
import 'package:rxdart/rxdart.dart';

import 'package:tabanan/src/data/common/models/string_model.dart';
import 'package:tabanan/src/data/school/models/index.dart';
import '../forms/register_form.dart';
import '../validators/user_validator.dart';
import '../repositories/user_repository.dart';


class RegisterBloc with UserValidator {
	final repository = UserRepository();
	
	final _username = BehaviorSubject<String>();
	final _email = BehaviorSubject<String>();
	final _password = BehaviorSubject<String>();
	final _registration = BehaviorSubject<StringModel>();
	
	Function(String) get setUsername => _username.sink.add;
	Stream<String> get username => _username.stream.transform(usernameValidatorTransformer);
	
	Function(String) get setEmail => _email.sink.add;
	Stream<String> get email => _email.stream.transform(emailValidatorTransformer);
	
	Function(String) get setPassword => _password.sink.add;
	Stream<String> get password => _password..stream.transform(passwordValidatorTransformer);
	
	Stream<bool> get validForm => Observable.combineLatest3<String, String, String, bool>(username, email, password, (u, e, p) { return true; });
	Stream<StringModel> get registration => _registration.stream;
	
	Future<Null> submit() async {
		final result = await repository.register(form: RegisterForm (
			_username.value,
			_email.value,
			_password.value,
		));
		_registration.sink.add(result);
	}
	
	dispose() {
		_username.close();
		_email.close();
		_password.close();
		_registration.close();
	}
}

//
//class RegisterProvider extends InheritedWidget {
//	final RegisterBloc bloc;
//
//	RegisterProvider({ Key key, Widget child, this.bloc })
//		: super(key: key, child: child);
//
//	@override
//	bool updateShouldNotify(RegisterProvider oldWidget) => oldWidget.bloc != this.bloc;
//
//	static RegisterProvider of(BuildContext context) => context.inheritFromWidgetOfExactType(RegisterProvider);
//}