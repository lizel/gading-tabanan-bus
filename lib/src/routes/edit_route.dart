import 'package:flutter/cupertino.dart';
import 'common_route.dart';
import '../screens/edit_screen/screen.dart';

class EditRoute extends CommonRoute {
	
	EditRoute({@required WidgetBuilder builder, RouteSettings settings, bool maintainState: false, bool fullscreenDialog: false }):
			super(builder: builder, settings: settings, maintainState: maintainState, fullscreenDialog: fullscreenDialog);
	
	factory EditRoute.build({ bool maintainState: true, bool fullscreenDialog: false }) {
		return EditRoute(
			builder: (BuildContext context) { return Screen(); },
			settings: RouteSettings(name: "/user/edit"),
			maintainState: maintainState,
			fullscreenDialog: fullscreenDialog
		);
	}
	
	@override
	Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) => builder(context);

	@override
	Duration get transitionDuration => const Duration(milliseconds: 300);

	@override
	Widget buildTransitions(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {

		return SlideTransition(
			position: new Tween<Offset> (
				begin: Offset(0.0, 1.0),
				end: Offset.zero,
			).animate(animation),
			child: child,
		);
		// return FadeTransition(opacity: animation, child: child);
	}
}