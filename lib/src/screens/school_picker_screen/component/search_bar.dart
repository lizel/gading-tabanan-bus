import 'package:flutter/material.dart';
import '../screen.dart';
import 'package:tabanan/src/components/state_data_widget.dart';

class SearchBar extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		final dw = StateDataWidget.of<ScreenState>(context);
		return Container(
			color: Colors.grey[200],
			padding: EdgeInsets.only(top: 30, bottom: 10),
			child: Row (
				crossAxisAlignment: CrossAxisAlignment.center,
				mainAxisSize: MainAxisSize.max,
				mainAxisAlignment: MainAxisAlignment.spaceEvenly,
				children: <Widget> [
					
					SizedBox(width: 10),
					
					RawMaterialButton(
						padding: EdgeInsets.all(10),
						child: Icon(Icons.arrow_back),
						shape: new CircleBorder(),
						onPressed: () => Navigator.pop(context, null),
						constraints: BoxConstraints(
							maxHeight: 55,
							maxWidth: 55
						),
					),
					
					SizedBox(width: 10),
					
					Flexible(
						child: TextField(
							onChanged: dw.state.bloc.searchFor,
							keyboardType: TextInputType.emailAddress,
							decoration: InputDecoration(
								filled: true,
								fillColor: Colors.grey[300],
								contentPadding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
								border: OutlineInputBorder(
									borderRadius: BorderRadius.circular(5),
									borderSide: BorderSide(
										width: 0,
										style: BorderStyle.none,
									),
								),
								hintText: "Cari Sekolah",
								suffixIcon: IconButton(
									icon: Icon(Icons.search),
									onPressed: null
								),
							),
						),
					),
					
					SizedBox(width: 20)
				
				],
			),
		);
	}
}