import 'dart:convert';
import 'package:http/http.dart' show Client;

import 'package:tabanan/src/data/config/configuration_bloc.dart';
import '../common/driver_provider.tiers.dart';
import '../models/index.dart';

class DriverApiProvider implements DriverSource {
	final _root = configurationBloc.endpoint.url;
	final _domain = configurationBloc.endpoint.domain;
	final client = new Client();
	
	Future<DriverModel> detail(String plat) async {
		final response = await client.get(Uri.https(_domain, "/tabanan/trans_serasi/get_driver_data.php", { "plat_no": plat }));
		final jsonResponse = json.decode(response.body);
		return DriverModel.fromJson(jsonResponse);
	}
	
}