import 'dart:async';
import 'package:validators/validators.dart';

abstract class UserValidator {
	final emailValidatorTransformer = StreamTransformer<String, String>.fromHandlers(
		handleData: (val, sink) {
			if (!isEmail(val)) { return sink.addError("Format email tidak benar. contoh: contoh@example.com"); }
			return sink.add(val);
		},
	);
	
	final usernameValidatorTransformer = StreamTransformer<String, String>.fromHandlers(
		handleData: (val, sink) {
			if (!isLength(val, 3, 50)) { return sink.addError("Banyaknya karakter yang diperbolehkan yaitu 3 - 50 karakter."); }
			if (!isAlphanumeric(val)) { return sink.addError("Hanya karakter alphabet dan angkat yang diperbolehkan."); }
			return sink.add(val);
		},
	);
	
	final passwordValidatorTransformer = StreamTransformer<String, String>.fromHandlers(
		handleData: (val, sink) {
			if (!isLength(val, 3, 20)) { return sink.addError("Banyaknya karakter yang diperbolehkan yaitu 3 - 20 karakter."); }
			return sink.add(val);
		},
	);
	
	final repeatPasswordValidatorTransformer = StreamTransformer<Map<String, String>, String>.fromHandlers(
		handleData: (val, sink) {
			if (val.values.length != 2) { return sink.addError("Password dan Konfirmasi Password tidak sama"); }
			if (val["password"] != val["repeat"]) { return sink.addError("Password dan Konfirmasi Password tidak sama"); }
			return sink.add(val["repeat"]);
		},
	);
}