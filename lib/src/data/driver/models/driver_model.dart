import 'package:json_annotation/json_annotation.dart';

part 'driver_model.g.dart';

@JsonSerializable()
class DriverModel {
	int value;
	List<DriverContent> message;
	
	DriverModel(this.value, this.message);
	
	factory DriverModel.fromJson(Map<String, dynamic> json) => _$DriverModelFromJson(json);
	Map<String, dynamic> toJson() => _$DriverModelToJson(this);
}

@JsonSerializable()
class DriverContent {
	String id;
	String kode;
	String nama;
	String alamat;
	String telp;
	String Foto;
	
	DriverContent(this.id, this.kode, this.nama, this.alamat, this.telp, this.Foto);
	
	factory DriverContent.fromJson(Map<String, dynamic> json) => _$DriverContentFromJson(json);
	Map<String, dynamic> toJson() => _$DriverContentToJson(this);
}