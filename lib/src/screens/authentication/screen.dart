import 'dart:async';
import 'package:flutter/material.dart';
import 'package:tabanan/src/data/user/blocs/authentication_bloc.dart';
import 'package:tabanan/src/routes/home_route.dart';
import 'package:tabanan/src/routes/login_route.dart';

import 'launch_preview.dart';

class AuthenticationScreen extends StatefulWidget {
	@override
	State createState() => AuthenticationScreenState();
}

class AuthenticationScreenState extends State<AuthenticationScreen> {
	final bloc = AuthenticationBloc();
	
	startTime () async {
		final _duration = Duration(milliseconds: 2500);
		return Timer(_duration, () => bloc.authenticate());
	}
	
	@override
	void initState() {
		super.initState();
		bloc
			.check
			.listen((ev) => Navigator.of(context).pushReplacement(!ev? LoginRoute.build(): HomeRoute.build()));
		
		startTime();
	}
	
	@override
	Widget build(BuildContext context) {
		return Scaffold(body: LaunchPreview());
	}
}