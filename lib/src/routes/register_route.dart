import 'package:flutter/material.dart';
import 'common_route.dart';
import '../screens/register_screen/register_screen.dart';

class RegisterRoute extends CommonRoute {
	
	RegisterRoute({@required WidgetBuilder builder, RouteSettings settings, bool maintainState: false, bool fullscreenDialog: false }):
			super(builder: builder, settings: settings, maintainState: maintainState, fullscreenDialog: fullscreenDialog);
	
	
	factory RegisterRoute.build({ RouteSettings settings, bool maintainState: true, bool fullscreenDialog: false }) {
		return RegisterRoute(
			builder: (BuildContext context) { return RegisterScreen(); },
			settings: RouteSettings(name: "/user/register"),
			maintainState: maintainState,
			fullscreenDialog: fullscreenDialog
		);
	}
	
//	@override
//	Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) => builder(context);
//
//	@override
//	Duration get transitionDuration => const Duration(milliseconds: 300);
//
	@override
	Widget buildTransitions(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {

		return SlideTransition(
			position: new Tween<Offset>(
				begin: Offset(0.0, 1.0),
				end: Offset.zero,
			).animate(animation),
			child: child,
		);
		// return FadeTransition(opacity: animation, child: child);
	}
}