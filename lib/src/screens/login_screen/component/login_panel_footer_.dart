import 'package:flutter/material.dart';
import '../../../routes/register_route.dart';

class LoginPanelFooter extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Container(
			height: 60,
			child: Center(
				child: Row(
					crossAxisAlignment: CrossAxisAlignment.center,
					mainAxisAlignment: MainAxisAlignment.center,
					children: <Widget>[
						Text("Belum punya akun? "),
						InkWell(
							onTap: () => Navigator.of(context).push(RegisterRoute.build()),
							child: Text("Daftar", style: TextStyle(color: Theme.of(context).colorScheme.primary, fontWeight: FontWeight.bold),),
						)
					],
				),
			)
		);
	}
}