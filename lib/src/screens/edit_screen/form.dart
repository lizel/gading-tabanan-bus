import 'dart:async';
import 'package:flutter/material.dart';
import 'package:tabanan/src/data/user/blocs/edit_bloc.dart';
import 'package:tabanan/src/components/common_dialog.dart';
import 'package:tabanan/src/data/common/models/string_model.dart';
import 'package:tabanan/src/data/user/models/index.dart';
import 'package:tabanan/src/routes/login_route.dart';
import 'package:tabanan/src/routes/home_route.dart';

class EditFormState extends State<EditForm>  {
	final bloc = EditBloc();
	final _emailTextController = TextEditingController();
	final _usernameTextController = TextEditingController();
	
	StreamSubscription<CredentialModel> credentialListener;
	
	@override
	void initState() {
		super.initState();
		credentialListener = bloc.credentialChannel.listen((val) {
			_usernameTextController.value = TextEditingValue(text: val.username);
			_emailTextController.value = TextEditingValue(text: val.email);
			bloc.setEmail(val.email);
			credentialListener.cancel();
		});
		
		bloc.logoutChannel.listen((_) => _logoutAction);
		bloc.submission.listen(_showRegistrationBuild);
		bloc.fetch();
	}
	
	_logoutAction() {
		Navigator.pushAndRemoveUntil(context, LoginRoute.build(), (route) => route.isFirst);
	}
	
	_showRegistrationBuild(StringModel snapshot) {
		return CommonDialog.showAlert(context, (context) {
			return AlertDialog(
				title: Text("Akun"),
				content: Column(
					mainAxisSize: MainAxisSize.min,
					crossAxisAlignment: CrossAxisAlignment.stretch,
					children: <Widget>[
						
						Text(snapshot.message),
						
						SizedBox(height: 20),
						
						SizedBox(
							height: 40,
							child: RaisedButton(
								onPressed: () => snapshot.value == 0? Navigator.pop(context): Navigator.pushAndRemoveUntil(context, HomeRoute.build(), (route) => route.settings.name == "/user/home"),
								child: Text("Ok"),
							),
						)
					
					],
				),
			);
		});
	}
	
	@override
	Widget build(BuildContext context) {
		return Column(
			crossAxisAlignment: CrossAxisAlignment.stretch,
			mainAxisAlignment: MainAxisAlignment.start,
			mainAxisSize: MainAxisSize.min,
			children: <Widget>[
				
				TextField (
					enabled: false,
					controller: _usernameTextController,
					decoration: InputDecoration(
						labelText: "User ID",
						hintText: "Input User ID terdaftar",
						hintStyle: Theme.of(context).textTheme.caption.copyWith(),
						suffixIcon: Icon(IconData(0xf345, fontFamily: "ionicons")),
					),
				),
				
				StreamBuilder(
					stream: bloc.email,
					builder: (context, snapshot) {
						return TextField (
							controller: _emailTextController,
							onChanged: bloc.setEmail,
							keyboardType: TextInputType.emailAddress,
							decoration: InputDecoration(
								labelText: "E-Mail",
								hintText: "Input E-Mail.",
								hintStyle: Theme.of(context).textTheme.caption.copyWith(),
								suffixIcon: Icon(IconData(0xF1B8, fontFamily: "ionicons")),
								errorText: snapshot.error,
								errorMaxLines: 2,
							),
						);
					},
				),
				
				StreamBuilder(
					stream: bloc.password,
					builder: (context, snapshot) {
						return TextField(
							onChanged: bloc.setPassword,
							decoration: InputDecoration(
								labelText: "Kata Sandi",
								hintText: "Input Kata Sandi.",
								hintStyle: Theme.of(context).textTheme.caption.copyWith(),
								hintMaxLines: 3,
								suffixIcon: Icon(IconData(0xf1b0, fontFamily: "ionicons")),
								errorText: snapshot.error,
								errorMaxLines: 2,
							),
							obscureText: true,
						);
					},
				),
				
				StreamBuilder(
					stream: bloc.repeatPassword,
					builder: (context, snapshot) {
						return TextField(
							onChanged: bloc.setPasswordRepeat,
							decoration: InputDecoration(
								labelText: "Konfirmasi",
								hintText: "Input Konfirmasi.",
								hintStyle: Theme.of(context).textTheme.caption.copyWith(),
								hintMaxLines: 3,
								suffixIcon: Icon(IconData(0xf1b0, fontFamily: "ionicons")),
								errorText: snapshot.error,
								errorMaxLines: 2,
							),
							obscureText: true,
						);
					},
				),
				
				SizedBox(height: 20),
				
				SizedBox(
					height: 40,
					child: StreamBuilder(
						stream: bloc.validForm,
						builder: (context, snapshot) {
							return RaisedButton (
								onPressed: snapshot.hasData? bloc.submit: null,
								child: Text("DAFTAR"),
								shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
							);
						},
					),
				),
			
			],
		);
	}
	
	@override
	void dispose() {
		bloc.dispose();
		super.dispose();
	}
}

class EditForm extends StatefulWidget {
	EditFormState createState() => EditFormState();
}



