import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class CommonRoute<T> extends PageRoute<T> {
	
	CommonRoute({
		@required this.builder,
		RouteSettings settings,
		this.maintainState = true,
		bool fullscreenDialog = false,
	}) : assert(builder != null),
			assert(maintainState != null),
			assert(fullscreenDialog != null),
			assert(opaque),
			super(settings: settings, fullscreenDialog: fullscreenDialog);
	
	final WidgetBuilder builder;
	
	@override
	final bool maintainState;
	
	@override
	Duration get transitionDuration => const Duration(milliseconds: 300);
	
	@override
	Color get barrierColor => null;
	
	@override
	String get barrierLabel => null;
	
	@override
	bool canTransitionFrom(TransitionRoute<dynamic> previousRoute) {
		return previousRoute is MaterialPageRoute || previousRoute is CupertinoPageRoute;
	}
	
	@override
	bool canTransitionTo(TransitionRoute<dynamic> nextRoute) {
		// Don't perform outgoing animation if the next route is a fullscreen dialog.
		return (nextRoute is MaterialPageRoute && !nextRoute.fullscreenDialog)
			|| (nextRoute is CupertinoPageRoute && !nextRoute.fullscreenDialog);
	}
	
	@override
	Widget buildPage(BuildContext context, Animation<double> animation,
		Animation<double> secondaryAnimation) {
		final Widget result = builder(context);
		assert(() {
			if (result == null) {
				throw FlutterError(
					'The builder for route "${settings.name}" returned null.\n'
						'Route builders must never return null.'
				);
			}
			return true;
		}());
		return Semantics(
			scopesRoute: true,
			explicitChildNodes: true,
			child: result,
		);
	}
	
	@override
	Widget buildTransitions(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
		final PageTransitionsTheme theme = Theme.of(context).pageTransitionsTheme;
		return theme.buildTransitions<T>(this, context, animation, secondaryAnimation, child);
	}
	
	@override
	String get debugLabel => '${super.debugLabel}(${settings.name})';
}


//class BasicRoute extends PageRouteBuilder {
//	final Widget widget;
//	BasicRoute({ @required this.widget, bool maintainState: true, bool fullscreenDialog: false }) : super(
//		settings: RouteSettings(name: "/user/register"),
//		pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) { return widget; },
//
//		transitionDuration: Duration(milliseconds: 350),
//		transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
//			return new SlideTransition(
//				position: new Tween<Offset>(
//					begin: const Offset(0.0, 1.0),
//					end: Offset.zero,
//				).animate(animation),
//				child: child,
//			);
//			// return FadeTransition(opacity: animation, child: child);
//		},
//
//		maintainState: maintainState,
//	);
//}