// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_form.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginForm _$LoginFormFromJson(Map<String, dynamic> json) {
  return LoginForm(
      username: json['username'] as String,
      password: json['password'] as String);
}

Map<String, dynamic> _$LoginFormToJson(LoginForm instance) => <String, dynamic>{
      'username': instance.username,
      'password': instance.password
    };
