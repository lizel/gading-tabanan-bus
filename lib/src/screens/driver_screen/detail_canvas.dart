import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:tabanan/src/data/driver/models/index.dart';

class DetailCanvas extends StatelessWidget {
	final DriverModel driver;
	final String platNo;
	
	DetailCanvas(this.driver, this.platNo);
	
	Future<bool> _launchSMS(BuildContext context) async {
		if (await canLaunch("sms:${driver.message[0].telp}")) {
			await launch("sms:${driver.message[0].telp}");
		} else {
			Clipboard.setData(ClipboardData(text: driver.message[0].telp));
			Scaffold.of(context).showSnackBar(SnackBar(content: Text("Nomor Telepon berhasil di salin")));
		}
		return true;
	}
	
	Future<bool> _launchCall(BuildContext context) async {
		if (await canLaunch("tel:${driver.message[0].telp}")) {
			await launch("tel:${driver.message[0].telp}");
		} else {
			Clipboard.setData(ClipboardData(text: driver.message[0].telp));
			Scaffold.of(context).showSnackBar(SnackBar(content: Text("Nomor Telepon berhasil di salin")));
		}
		return true;
	}
	
	@override
	Widget build(BuildContext context) {
		return Row (
			
			children: <Widget>[
		
				ClipOval(
					child: FadeInImage(
						image: NetworkImage("https://golpay.io/tabanan/photos/${driver.message[0].Foto}"),
						placeholder: AssetImage("assets/images/app/logo.png"),
						fit: BoxFit.fill,
						width: 40,
					),
				),
				
				SizedBox(width: 10),
				
				Flexible(
					child: Column(
						crossAxisAlignment: CrossAxisAlignment.stretch,
						mainAxisAlignment: MainAxisAlignment.start,
						mainAxisSize: MainAxisSize.min,
						children: <Widget>[
							Text(driver.message[0].nama, style: Theme.of(context).textTheme.title.copyWith(), maxLines: 1),
							Text("No. Polisi: $platNo", style: Theme.of(context).textTheme.caption.copyWith(), maxLines: 1),
						],
					
					),
				),
				
				Row (
					crossAxisAlignment: CrossAxisAlignment.start,
					mainAxisAlignment: MainAxisAlignment.start,
					mainAxisSize: MainAxisSize.min,
					children: <Widget> [
						
						IconButton(
							icon: Icon(Icons.call),
							onPressed: () => _launchCall(context),
						),
						
						IconButton(
							icon: Icon(Icons.message),
							onPressed: () => _launchSMS(context),
						),
					
					],
				
				),
			
			
			],
		
		);
	}
}