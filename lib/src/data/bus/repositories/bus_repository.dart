import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

import '../common/bus_provider_tiers.dart';
import '../models/index.dart';
import '../providers/index.dart';

class BusRepository {
	final List<BusSource> sources = [ BusApiProvider() ];
	
	Future<List<BusContent>> list() {
		return sources[0].list();
	}
	
	Future<BusContent> fetch(@required String plat) async {
		final result = await sources[0].fetch(plat: plat);
		return result.length == 0? null: result.first;
	}
	
	Future<List<BusMovementContent>> track ({ String plat, DateTime start, DateTime end }) {
		DateFormat formatter = DateFormat("yyyyMMddHHmmss");
		final dateStart = start != null? formatter.format(start): null;
		final dateEnd = end != null? formatter.format(end): null;
		return sources[0].track(plat: plat, start: dateStart, end: dateEnd);
	}
}