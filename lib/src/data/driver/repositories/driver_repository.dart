import 'package:meta/meta.dart';

import '../common/driver_provider.tiers.dart';
import '../models/index.dart';
import '../providers/index.dart';

class DriverRepository {
	final List<DriverSource> sources = [ DriverApiProvider() ];
	
	Future<DriverModel> detail(@required String plat) {
		return sources[0].detail(plat);
	}
}