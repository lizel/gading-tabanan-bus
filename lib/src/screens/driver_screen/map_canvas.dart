import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:tabanan/src/data/driver/blocs/driver_bloc.dart';
import 'package:tabanan/src/data/driver/models/index.dart';
import 'package:tabanan/src/data/bus/models/index.dart';

class MapCanvasState extends State<MapCanvas> {
	final _defaultCameraPosition = CameraPosition(
		target: LatLng(-8.340539, 115.091949),
		zoom: 15,
	);
	
	final Set<Marker> _markers = {};
	final Completer<GoogleMapController> _controller = Completer();
	final DriverBloc bloc;
	final String platNo;
	
	MapCanvasState(this.bloc, this.platNo);
	
	@override
	void initState() {
		super.initState();
		bloc.busDetailChannel.listen((bus) {
			_controller.future.then((ctrl) {
				setState(() {
					_drawBusDetail(bus);
					if (bus.log_positions.length > 0) {
						ctrl.animateCamera(CameraUpdate.newLatLngZoom(LatLng(bus.log_positions[0].lat, bus.log_positions[0].lng), 14));
					}
				});
			});
		});
		bloc.track();
	}
	
	_drawBusDetail(BusMovementContent bus) {
		_markers.clear();
		bus.log_positions.forEach((log) {
			final coord = LatLng(log.lat, log.lng);
			var bitmap = BitmapDescriptor.fromAsset("assets/images/app/steps_${log.no}.png");
			if (log.no == 1) {
				if (log.status_engine) { bitmap = BitmapDescriptor.fromAsset("assets/images/app/bus_marker_icon.png"); }
				else { bitmap = BitmapDescriptor.fromAsset("assets/images/app/bus_imarker_icon.png"); }
			}
			
			_markers.add(Marker(
				markerId: MarkerId("${log.no}"),
				infoWindow: InfoWindow(
					title: "${log.no}. Waktu",
					snippet: "${log.date_time.hour}:${log.date_time.minute}:${log.date_time.second}"
				),
				position: coord,
				icon: bitmap
			));
		});
	}
	
	@override
	Widget build(BuildContext context) {
		return Stack(
			fit: StackFit.expand,
			children: <Widget>[
				
				Positioned.fill(
					child: GoogleMap(
						mapType: MapType.normal,
						initialCameraPosition: _defaultCameraPosition,
						onMapCreated: _controller.complete,
						markers: _markers,
					)
				),
		
				Positioned(
					right: 0,
					bottom: 0,
					child: Container(
						padding: EdgeInsets.all(20),
						child: Container(
							child: Container(
								padding: EdgeInsets.all(10),
								child: StreamBuilder<BusMovementContent>(
									stream: bloc.busDetailChannel,
									builder: (context, snapshot) {
										var distance = snapshot.hasData? snapshot.data.total_distance ?? 0: 0;
										final distancePrecision = distance.toStringAsPrecision(2);
										return Row(
											crossAxisAlignment: CrossAxisAlignment.center,
											mainAxisSize: MainAxisSize.min,
											children: <Widget>[
												Icon(Icons.location_on),
												SizedBox(width: 10),
												Text("$distancePrecision KM")
											],
										);
									}
								),
								decoration: BoxDecoration(
									color: Colors.white,
									borderRadius: BorderRadius.circular(20),
								),
							),
						),
					),
				)
				
			],
		);
	}
}

class MapCanvas extends StatefulWidget {
	final DriverBloc bloc;
	final String platNo;
	
	MapCanvas(this.bloc, this.platNo);
	
	@override
  	State<StatefulWidget> createState() => MapCanvasState(bloc, platNo);
}