import 'dart:async';
import 'package:flutter/material.dart';

import 'package:tabanan/src/components/state_data_widget.dart';
import 'package:tabanan/src/data/driver/blocs/driver_bloc.dart';
import 'package:tabanan/src/data/driver/models/index.dart';
import 'package:tabanan/src/routes/home_route.dart';

import 'detail_canvas.dart';
import 'map_canvas.dart';

class ScreenState extends State<Screen> {
	final String platNo;
	DriverBloc bloc;
	
	ScreenState(this.platNo) {
		bloc = DriverBloc(platNo: platNo);
	}
	
	@override
	void initState() {
		super.initState();
		bloc.track();
		bloc.detail();
	}
	
	@override
	Widget build(BuildContext context) {
		return StateDataWidget<ScreenState>(
			child: Scaffold (

				body: Column (
					children: <Widget>[
						
						Flexible(
							child: MapCanvas(bloc, platNo),
						),
						
						Container(
							padding: EdgeInsets.all(10),
							child: Container (
								padding: EdgeInsets.all(10),
								child: Column (
									crossAxisAlignment: CrossAxisAlignment.stretch,
									mainAxisAlignment: MainAxisAlignment.start,
									mainAxisSize: MainAxisSize.min,
									children: <Widget>[
										
										StreamBuilder(
											stream: bloc.driverChannel,
											builder: (BuildContext context, AsyncSnapshot<DriverModel> snapshot) {
												if (snapshot.hasData) {
													return DetailCanvas(snapshot.data, platNo);
												}
												
												if  (snapshot.hasError) {
													return Container(
														height: 75,
														alignment: Alignment(0, 0),
														child: Text("Driver belum terdaftar", style: Theme.of(context).textTheme.body2),
													);
												}
												
												return Container(
													height: 75,
													alignment: Alignment(0, 0),
													child: CircularProgressIndicator(),
												);
											}
										),
										
										Divider(),
										
										Row (
											crossAxisAlignment: CrossAxisAlignment.center,
											mainAxisAlignment: MainAxisAlignment.start,
											mainAxisSize: MainAxisSize.max,
											children: <Widget>[
												
												Flexible (
													child: Container (
														height: 40,
														child: Row (
															crossAxisAlignment: CrossAxisAlignment.center,
															mainAxisAlignment: MainAxisAlignment.start,
															mainAxisSize: MainAxisSize.max,
															children: <Widget>[
																
																StreamBuilder (
																	stream: bloc.startDateAssignmentChannel,
																	builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
																		return Text (snapshot.data ?? "Tanggal Awal", style: Theme.of(context).textTheme.body1);
																	}
																),
																
																Flexible(flex: 1, child: Container()),
																IconButton(
																	icon: Icon(Icons.alarm),
																	onPressed: () {
																		showDatePicker(
																			context: context,
																			initialDate: DateTime.now(),
																			firstDate: DateTime.now().subtract(Duration(days: 2)),
																			lastDate: DateTime.now()
																		).then((dateSelected) {
																			showTimePicker(context: context, initialTime: TimeOfDay.now())
																				.then((timeSelected) => bloc.setStartDate(dateSelected, timeSelected));
																		});
																	},
																)
															],
														),
													),
												),
												
												Flexible(
													child: Container (
														height: 40,
														child: Row (
															crossAxisAlignment: CrossAxisAlignment.center,
															mainAxisAlignment: MainAxisAlignment.start,
															mainAxisSize: MainAxisSize.max,
															children: <Widget>[
																StreamBuilder (
																	stream: bloc.endDateAssignmentChannel,
																	builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
																		return Text (snapshot.data ?? "Tanggal Akhir", style: Theme.of(context).textTheme.body1);
																	}
																),
																
																Flexible(flex: 1, child: Container()),
																
																IconButton(
																	icon: Icon(Icons.alarm),
																	onPressed: () {
																		final now = DateTime.now();
																		showDatePicker(
																			context: context,
																			initialDate: now,
																			firstDate: DateTime.now().subtract(Duration(days: 2)),
																			lastDate: now
																		).then((dateSelected) {
																			showTimePicker(context: context, initialTime: TimeOfDay.now())
																				.then((timeSelected) => bloc.setEndDate(dateSelected, timeSelected));
																		});
																	},
																)
															],
														),
													),
												),
											
											],
										),
										
										SizedBox(height: 20),
										
										SizedBox(
											child: RaisedButton(
												onPressed: () => Navigator.pushAndRemoveUntil(context, HomeRoute.build(), (route) => route.isFirst),
												child: Text("KEMBALI"),
												shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
											),
											width: double.infinity,
											height: 40,
										),
									
									],
								),
							),
						)
						
					],
				)
				
			),
		);
	}
	
	@override
	void dispose() {
		bloc.dispose();
		super.dispose();
	}
}

class Screen extends StatefulWidget {
	final String platNo;
	Screen(this.platNo);
	
	@override
	State createState() => ScreenState(platNo);
}
