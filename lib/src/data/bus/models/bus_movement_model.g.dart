// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bus_movement_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BusMovementModel _$BusMovementModelFromJson(Map<String, dynamic> json) {
  return BusMovementModel((json['message'] as List)
      ?.map((e) => e == null
          ? null
          : BusMovementContent.fromJson(e as Map<String, dynamic>))
      ?.toList());
}

Map<String, dynamic> _$BusMovementModelToJson(BusMovementModel instance) =>
    <String, dynamic>{'message': instance.message};

BusMovementContent _$BusMovementContentFromJson(Map<String, dynamic> json) {
  return BusMovementContent(
      json['vehicle_id'] as int,
      json['license_plate_no'] as String,
      json['qty_positions'] as int,
      (json['total_distance'] as num)?.toDouble(),
      (json['log_positions'] as List)
          ?.map((e) => e == null
              ? null
              : BusMovementLog.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      json['merk'] as String,
      json['type'] as String,
      json['halo'] as String,
      json['company'] as String);
}

Map<String, dynamic> _$BusMovementContentToJson(BusMovementContent instance) =>
    <String, dynamic>{
      'vehicle_id': instance.vehicle_id,
      'license_plate_no': instance.license_plate_no,
      'qty_positions': instance.qty_positions,
      'total_distance': instance.total_distance,
      'log_positions': instance.log_positions,
      'merk': instance.merk,
      'type': instance.type,
      'halo': instance.halo,
      'company': instance.company
    };

BusMovementLog _$BusMovementLogFromJson(Map<String, dynamic> json) {
  return BusMovementLog(
      json['no'] as int,
      (json['lat'] as num)?.toDouble(),
      (json['lng'] as num)?.toDouble(),
      json['date_time'] == null
          ? null
          : DateTime.parse(json['date_time'] as String),
      json['status_engine'] as bool);
}

Map<String, dynamic> _$BusMovementLogToJson(BusMovementLog instance) =>
    <String, dynamic>{
      'no': instance.no,
      'lat': instance.lat,
      'lng': instance.lng,
      'date_time': instance.date_time?.toIso8601String(),
      'status_engine': instance.status_engine
    };
