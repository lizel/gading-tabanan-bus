// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'foxlogger.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FoxloggerConfiguration _$FoxloggerConfigurationFromJson(
    Map<String, dynamic> json) {
  return FoxloggerConfiguration(json['url'] as String, json['key'] as String);
}

Map<String, dynamic> _$FoxloggerConfigurationToJson(
        FoxloggerConfiguration instance) =>
    <String, dynamic>{'url': instance.url, 'key': instance.key};

// **************************************************************************
// JsonLiteralGenerator
// **************************************************************************

const _$foxloggerMapsJsonLiteral = {
  'key': '4cd78db6-f78e-11e8-a05b-1866daeaa2ac',
  'url': 'terminalapi.foxlogger.info'
};
