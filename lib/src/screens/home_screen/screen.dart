import 'package:flutter/material.dart';

import 'package:tabanan/src/components/state_data_widget.dart';
import 'package:tabanan/src/data/bus/blocs/home_bloc.dart';

import 'drawers/drawer.dart';
import 'search_bar.dart';
import 'float_button.dart';
import 'map_canvas.dart';

class Screen extends StatefulWidget {
	@override
	State createState() => ScreenState();
}

class ScreenState extends State<Screen> {
	final bloc = HomeBloc();
	
	@override
	Widget build(BuildContext context) {
		return StateDataWidget<ScreenState>(
			child: Scaffold (
//				drawer: HomeDrawer(),
				body: Stack(
					children: <Widget>[
						Positioned (
							top: 0, bottom: 0,
							left: 0, right: 0,
							child: MapCanvas(bloc)
						),
						Positioned (
							bottom: 0,
							left: 0, right: 0,
							child: SearchBar(bloc),
						)
					],
				)
			),
		);
	}
	
	@override
	void dispose() {
		bloc.dispose();
		super.dispose();
	}
}