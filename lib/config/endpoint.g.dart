// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EndpointConfiguration _$EndpointConfigurationFromJson(
    Map<String, dynamic> json) {
  return EndpointConfiguration(json['url'] as String, json['domain'] as String);
}

Map<String, dynamic> _$EndpointConfigurationToJson(
        EndpointConfiguration instance) =>
    <String, dynamic>{'url': instance.url, 'domain': instance.domain};

// **************************************************************************
// JsonLiteralGenerator
// **************************************************************************

const _$endpointMapsJsonLiteral = {
  'domain': 'www.elisoft.id',
  'url': 'https://www.elisoft.id'
};
