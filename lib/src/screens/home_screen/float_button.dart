import 'package:flutter/material.dart';
import 'bottom_sheet/bottom_sheet.dart';

class FloatButton extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Container (
			alignment: Alignment(0, 0.8),
			height: 150,
			child: Container (
				width: 60,
				height: 60,
				padding: EdgeInsets.all(1),
				decoration: BoxDecoration(
					color: Colors.white,
//					borderRadius: BorderRadius.circular(9),
					shape: BoxShape.circle,
					boxShadow: [
						BoxShadow(
							color: Colors.black54,
							blurRadius: 10.0,
							spreadRadius: 2.0,
							offset: Offset(0, 1)
						),
					],
				),
				child: Material(
					color: Colors.transparent,
					child: Ink.image(
						image: AssetImage('assets/images/app/logo.png'),
						fit: BoxFit.contain,
						child: InkWell(
							onTap: () => showModalBottomSheet(context: context, builder: (_) => HomeBottomSheet()),
						)
					),
				),
			)
		);
	}
}