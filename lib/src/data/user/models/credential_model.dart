import 'package:json_annotation/json_annotation.dart';

part 'credential_model.g.dart';

@JsonSerializable()
class CredentialModel {
	String username;
	String email;
	String password;
	String KodeSekolah;
	String NamaSekolah;
	
	
	CredentialModel(this.username, this.email, this.password, this.KodeSekolah,
		this.NamaSekolah);
	
	factory CredentialModel.fromJson(Map<String, dynamic> json) => _$CredentialModelFromJson(json);
	Map<String, dynamic> toJson() => _$CredentialModelToJson(this);
}