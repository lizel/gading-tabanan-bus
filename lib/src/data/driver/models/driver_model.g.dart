// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'driver_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DriverModel _$DriverModelFromJson(Map<String, dynamic> json) {
  return DriverModel(
      json['value'] as int,
      (json['message'] as List)
          ?.map((e) => e == null
              ? null
              : DriverContent.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$DriverModelToJson(DriverModel instance) =>
    <String, dynamic>{'value': instance.value, 'message': instance.message};

DriverContent _$DriverContentFromJson(Map<String, dynamic> json) {
  return DriverContent(
      json['id'] as String,
      json['kode'] as String,
      json['nama'] as String,
      json['alamat'] as String,
      json['telp'] as String,
      json['Foto'] as String);
}

Map<String, dynamic> _$DriverContentToJson(DriverContent instance) =>
    <String, dynamic>{
      'id': instance.id,
      'kode': instance.kode,
      'nama': instance.nama,
      'alamat': instance.alamat,
      'telp': instance.telp,
      'Foto': instance.Foto
    };
