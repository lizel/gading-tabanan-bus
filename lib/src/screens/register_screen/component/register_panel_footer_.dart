import 'package:flutter/material.dart';

class RegisterPanelFooter extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Container(
			height: 60,
			child: Center(
				child: Row(
					mainAxisAlignment: MainAxisAlignment.center,
					mainAxisSize: MainAxisSize.min,
					crossAxisAlignment: CrossAxisAlignment.start,
					children: <Widget>[
						Text("Sudah punya akun? "),
						InkWell(
							onTap: () => Navigator.pop(context),
							child: Text("Login", style: TextStyle(color: Theme.of(context).colorScheme.primary, fontWeight: FontWeight.bold),),
						)
					],
				),
			)
		);
	}
}