import 'package:json_annotation/json_annotation.dart';

part 'foxlogger.g.dart';

@JsonSerializable()
class FoxloggerConfiguration {
	String url;
	String key;
	
	FoxloggerConfiguration(this.url, this.key);
	
	factory FoxloggerConfiguration.fromJson(Map<String, dynamic> json) => _$FoxloggerConfigurationFromJson(json);
	Map<String, dynamic> toJson() => _$FoxloggerConfigurationToJson(this);
}

@JsonLiteral("foxlogger.json", asConst: true)
Map<String, dynamic> get foxloggerMaps => _$foxloggerMapsJsonLiteral;
final FoxloggerConfiguration foxloggerConfiguration = FoxloggerConfiguration.fromJson(foxloggerMaps);