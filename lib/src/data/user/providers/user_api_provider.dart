import 'dart:convert';
import 'package:http/http.dart' show Client;

import '../common/user_provider_tiers.dart';
import 'package:tabanan/src/data/config/configuration_bloc.dart';
import 'package:tabanan/src/data/common/models/string_model.dart';
import '../forms/index.dart';
import '../models/index.dart';

class UserApiProvider implements UserSource {
	final _root = configurationBloc.endpoint.url;
	final _domain = configurationBloc.endpoint.domain;
	final client = new Client();
	
	Future<StringModel> login({ LoginForm form }) async {
		final response = await client.post("$_root/tabanan/trans_serasi/login.php", body: form.toJson(), encoding: Utf8Codec());
		final jsonResponse = json.decode(response.body);
		return StringModel.fromJson(jsonResponse);
	}
	
	Future<StringModel> register({ RegisterForm form }) async {
		final response = await client.post("$_root/tabanan/trans_serasi/insert_user.php", body: form.toJson(), encoding: Utf8Codec());
		final jsonResponse = json.decode(response.body);
		return StringModel.fromJson(jsonResponse);
	}
	
	Future<StringModel> edit({ RegisterForm form }) async {
		final response = await client.post("$_root/tabanan/trans_serasi/set_user_data.php", body: form.toJson(), encoding: Utf8Codec());
		final jsonResponse = json.decode(response.body);
		return StringModel.fromJson(jsonResponse);
	}
	
	Future<StringModel> reset({ String username }) async {
		final response = await client.post("$_root/tabanan/trans_serasi/send_email_change_passwd.php", body: { "username": username }, encoding: Utf8Codec());
		final jsonResponse = json.decode(response.body);
		return StringModel.fromJson(jsonResponse);
	}
	
	Future<DetailModel> detail({ String username }) async {
		final response = await client.get(Uri.https(_domain, "/tabanan/trans_serasi/get_user_data.php", { "username": username }));
		final jsonResponse = json.decode(response.body);
		return DetailModel.fromJson(jsonResponse);
	}
}