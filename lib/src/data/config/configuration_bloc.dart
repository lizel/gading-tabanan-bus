import 'package:tabanan/config/endpoint.dart';
import 'package:tabanan/config/foxlogger.dart';

class ConfigurationBloc {
	final EndpointConfiguration endpoint = endpointConfiguration;
	final FoxloggerConfiguration foxlogger = foxloggerConfiguration;
}

final configurationBloc = ConfigurationBloc();