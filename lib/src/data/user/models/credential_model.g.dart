// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credential_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CredentialModel _$CredentialModelFromJson(Map<String, dynamic> json) {
  return CredentialModel(
      json['username'] as String,
      json['email'] as String,
      json['password'] as String,
      json['KodeSekolah'] as String,
      json['NamaSekolah'] as String);
}

Map<String, dynamic> _$CredentialModelToJson(CredentialModel instance) =>
    <String, dynamic>{
      'username': instance.username,
      'email': instance.email,
      'password': instance.password,
      'KodeSekolah': instance.KodeSekolah,
      'NamaSekolah': instance.NamaSekolah
    };
