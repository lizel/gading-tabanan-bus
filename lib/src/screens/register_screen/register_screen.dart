import 'package:flutter/material.dart';
import 'component/register_panel_body.dart';
import 'component/register_panel_header.dart';
import 'component/register_panel_footer_.dart';

class RegisterScreen extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			body: LayoutBuilder(
				builder: (BuildContext context, BoxConstraints viewportConstraints) {
					return SingleChildScrollView(
						child: ConstrainedBox(
							constraints: BoxConstraints(minHeight: viewportConstraints.maxHeight),
							child: Container(
								padding: EdgeInsets.symmetric(horizontal: 20),
								child: Column(
									crossAxisAlignment: CrossAxisAlignment.stretch,
									mainAxisAlignment: MainAxisAlignment.spaceBetween,
									mainAxisSize: MainAxisSize.max,
									children: <Widget>[
										RegisterPanelHeader(),
										
										Column(
											crossAxisAlignment: CrossAxisAlignment.stretch,
											mainAxisAlignment: MainAxisAlignment.start,
											mainAxisSize: MainAxisSize.min,
											children: <Widget>[
												RegisterPanelBody(),
												RegisterPanelFooter()
											],
										)
									],
								),
							),
						),
					);
				}
			)
		);
	}
}

