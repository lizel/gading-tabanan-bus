import 'package:flutter/material.dart';
import 'register_panel_body_form.dart';

class RegisterPanelBody extends StatelessWidget {
	@override
	Widget build(BuildContext context) {
		return Container(
			constraints: BoxConstraints(minHeight: 320),
			padding: EdgeInsets.symmetric(horizontal: 15),
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.stretch,
				mainAxisAlignment: MainAxisAlignment.start,
				mainAxisSize: MainAxisSize.min,
				children: <Widget>[
					Column(
						crossAxisAlignment: CrossAxisAlignment.start,
						mainAxisAlignment: MainAxisAlignment.start,
						children: <Widget>[
							Text("Daftar", style: Theme.of(context).textTheme.display1.copyWith(color: Colors.black, fontWeight: FontWeight.bold)),
							SizedBox(height: 10),
							Text("Daftarkan diri anda untuk menggunakan aplikasi.", style: Theme.of(context).textTheme.caption.copyWith(color: Colors.black54)),
						],
					),
					
					RegisterPanelBodyForm()
				],
			),
		);
	}
}