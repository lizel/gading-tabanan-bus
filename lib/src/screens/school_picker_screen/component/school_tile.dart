import 'package:flutter/material.dart';
import 'package:tabanan/src/data/school/models/index.dart';

class SchoolTile extends StatelessWidget {
	final SchoolContent model;
	final Map<String, String> tipeToImageMap = {
		"SD": "assets/images/app/sd_button.png",
		"SMP": "assets/images/app/smp_button.png",
		"SMA": "assets/images/app/sma_button.png",
	};
	
	SchoolTile(this.model);
	
	@override
	Widget build(BuildContext context) {
		final imagePath = tipeToImageMap.containsKey(model.Tipe)? tipeToImageMap[model.Tipe]: "assets/images/app/logo.png";
		return Material(
			color: Color(0),
			child: InkWell(
				onTap: () => Navigator.pop(context, model),
				child: Container(
					height: 80,
					padding: EdgeInsets.only(left: 10, right: 0, top: 10, bottom: 10),
					child: Row(
						crossAxisAlignment: CrossAxisAlignment.center,
						mainAxisSize: MainAxisSize.max,
						mainAxisAlignment: MainAxisAlignment.start,
						children: <Widget>[
							
							SizedBox(
								width: 40,
								height: 40,
								child: Image.asset(imagePath),
							),
							
							SizedBox(width: 15),
							
							Flexible(
								child: Column(
									crossAxisAlignment: CrossAxisAlignment.stretch,
									mainAxisAlignment: MainAxisAlignment.start,
									mainAxisSize: MainAxisSize.min,
									children: <Widget>[
										Text(model.NamaSekolah.toUpperCase(), style: Theme.of(context).textTheme.body1.copyWith(color: Colors.black, fontWeight: FontWeight.bold), maxLines: 1),
										Text(model.AlamatSekolah, maxLines: 2)
									],
								),
							),
							
							SizedBox(width: 10),
							
							IconButton(
								onPressed: () {},
								icon: Icon(Icons.chevron_right),
							)
						
						],
					),
					decoration: BoxDecoration(
//					color: Colors.white,
						border: Border(
							bottom: BorderSide(
								color: Colors.grey[300],
								width: 1
							)
						),
//					borderRadius: BorderRadius.circular(4),
					),
				),
			),
		);
	}
	
}