import 'package:flutter/cupertino.dart';
import 'common_route.dart';
import '../screens/driver_screen/screen.dart';

class DriverRoute extends CommonRoute {
	
	DriverRoute({@required WidgetBuilder builder, RouteSettings settings, bool maintainState: false, bool fullscreenDialog: false }):
			super(builder: builder, settings: settings, maintainState: maintainState, fullscreenDialog: fullscreenDialog);
	
	factory DriverRoute.build({ @required String platNo,  bool maintainState: true, bool fullscreenDialog: false }) {
		return DriverRoute(
			builder: (BuildContext context) { return Screen(platNo); },
			settings: RouteSettings(name: "/user/driver"),
			maintainState: maintainState,
			fullscreenDialog: fullscreenDialog
		);
	}
	
	@override
	Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) => builder(context);

	@override
	Duration get transitionDuration => const Duration(milliseconds: 300);

	@override
	Widget buildTransitions(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {

		return SlideTransition(
			position: new Tween<Offset> (
				begin: Offset(0.0, 1.0),
				end: Offset.zero,
			).animate(animation),
			child: child,
		);
		// return FadeTransition(opacity: animation, child: child);
	}
}