import 'package:flutter/material.dart';

abstract class CommonDialog {

	static Future<T> showLoadingDialog<T>(BuildContext context) {
		return showGeneralDialog(
			context: context,
			pageBuilder: (BuildContext buildContext, Animation<double> animation, Animation<double> secondaryAnimation) {
				return SafeArea(
					child: Builder(
						builder: (BuildContext context) {
							return Dialog(
								elevation: 0,
								backgroundColor: Color(0),
								child: Column(
									crossAxisAlignment: CrossAxisAlignment.center,
									mainAxisSize: MainAxisSize.min,
									children: <Widget>[
										Container(
											constraints: BoxConstraints(maxHeight: 75, maxWidth: 75),
											alignment: Alignment(0, 0),
											child: ConstrainedBox(
												constraints: BoxConstraints(maxWidth: 50, maxHeight: 50),
												child: CircularProgressIndicator(),
											),
											decoration: BoxDecoration(
												boxShadow: [
													BoxShadow(
														color: Colors.black54,
														blurRadius: 50.0,
														spreadRadius: 5.0,
														offset: Offset.zero
													),
												],
												color: Colors.white,
												borderRadius: BorderRadius.circular(10)
											),
										),
									],
								),
							);
						}
					),
				);
			},
			barrierDismissible: false,
			barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
			barrierColor: Colors.white54,
			transitionDuration: const Duration(milliseconds: 150),
			//transitionBuilder: _buildMaterialDialogTransitions,
		);
	}
	
	static Future<T> showAlert<T>(BuildContext context, WidgetBuilder builder) {
		return showGeneralDialog(
			context: context,
			pageBuilder: (BuildContext buildContext, Animation<double> animation, Animation<double> secondaryAnimation) => SafeArea(child: Builder(builder: builder)),
			barrierDismissible: true,
			barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
			barrierColor: Colors.white54,
			transitionDuration: const Duration(milliseconds: 150),
			//transitionBuilder: _buildMaterialDialogTransitions,
		);
	}
	
}