// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'string_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StringModel _$StringModelFromJson(Map<String, dynamic> json) {
  return StringModel(
      value: json['value'] as int, message: json['message'] as String);
}

Map<String, dynamic> _$StringModelToJson(StringModel instance) =>
    <String, dynamic>{'value': instance.value, 'message': instance.message};
