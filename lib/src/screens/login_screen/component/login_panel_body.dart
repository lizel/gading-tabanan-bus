import 'package:flutter/material.dart';
import 'form.dart';

class LoginPanelBody extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Container(
			constraints: BoxConstraints(minHeight: 300),
			padding: EdgeInsets.symmetric(horizontal: 15),
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.stretch,
				mainAxisAlignment: MainAxisAlignment.start,
				mainAxisSize: MainAxisSize.min,
				children: <Widget>[
					Column(
						crossAxisAlignment: CrossAxisAlignment.start,
						mainAxisAlignment: MainAxisAlignment.start,
						children: <Widget>[
							Text("LOGIN", style: Theme.of(context).textTheme.display1.copyWith(color: Colors.black, fontWeight: FontWeight.bold)),
							SizedBox(height: 10),
							Text("Untuk menggunakan aplikasi silahkan login.", style: Theme.of(context).textTheme.caption.copyWith(color: Colors.black54)),
						],
					),
					LoginForm()
				],
			),
		);
	}
}