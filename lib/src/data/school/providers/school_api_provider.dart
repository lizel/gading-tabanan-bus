import 'dart:convert';
import 'package:http/http.dart' show Client;

import '../common/school_provider_tiers.dart';
import 'package:tabanan/src/data/config/configuration_bloc.dart';
import 'package:tabanan/src/data/common/models/string_model.dart';
//import '../forms/index.dart';
import '../models/index.dart';

class SchoolApiProvider implements SchoolSource {
	final _root = configurationBloc.endpoint.url;
	final _domain = configurationBloc.endpoint.domain;
	final client = new Client();
	
	Future<SchoolModel> list() async {
		final response = await client.get(Uri.https(_domain, "/tabanan/trans_serasi/get_schools.php"));
		final jsonResponse = json.decode(response.body);
		return SchoolModel.fromJson(jsonResponse);
	}
	
}