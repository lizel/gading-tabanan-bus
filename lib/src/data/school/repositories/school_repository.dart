import '../common/school_provider_tiers.dart';
//import '../forms/index.dart';
import '../models/index.dart';
import '../providers/index.dart';

class SchoolRepository {
	final List<SchoolSource> sources = [ SchoolApiProvider() ];
	
	Future<SchoolModel> list() {
		return sources[0].list();
	}

}