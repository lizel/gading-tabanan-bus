import 'package:flutter/material.dart';
import 'common_route.dart';
import '../screens/home_screen/screen.dart';

class HomeRoute extends CommonRoute {
	
	HomeRoute({@required WidgetBuilder builder, RouteSettings settings, bool maintainState: false, bool fullscreenDialog: false }):
			super(builder: builder, settings: settings, maintainState: maintainState, fullscreenDialog: fullscreenDialog);
	
	
	factory HomeRoute.build({ RouteSettings settings, bool maintainState: true, bool fullscreenDialog: false }) {
		return HomeRoute(
			builder: (BuildContext context) { return Screen(); },
			settings: RouteSettings(name: "/user/home"),
			maintainState: maintainState,
			fullscreenDialog: fullscreenDialog
		);
	}
	
	@override
	Widget buildTransitions(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
		return SlideTransition(
			position: new Tween<Offset>(
				begin: Offset(0.0, 1.0),
				end: Offset.zero,
			).animate(animation),
			child: child,
		);
	}
}