import 'dart:async';
import 'package:flutter/material.dart';
import 'package:tabanan/src/components/state_data_widget.dart';
import 'package:tabanan/src/data/school/models/index.dart';
import '../screen.dart';
import 'school_tile.dart';

class Body extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		final dw = StateDataWidget.of<ScreenState>(context);
		return Container(
			color: Colors.grey[200],
			padding: EdgeInsets.all(20),
			constraints: BoxConstraints.expand(),
			child: Container(
				child: StreamBuilder(
					stream: dw.state.bloc.check,
					builder: (BuildContext context, AsyncSnapshot<List<SchoolContent>> snapshot) {
						
						if (!snapshot.hasData) {
							return Center(child: CircularProgressIndicator());
						}
						
						if (snapshot.hasData) {
							return ListView.builder(
								padding: EdgeInsets.all(0),
								itemCount: snapshot.data.length,
								itemBuilder: (BuildContext context, int index) {
									return SchoolTile(snapshot.data[index]);
								}
							);
						}
					}
				),
			)
		);
	}
	
}