import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../models/index.dart';
import '../repositories/index.dart';
import 'package:tabanan/src/data/school/models/index.dart';

class HomeBloc {
	final storage = new FlutterSecureStorage();
	
	final _repository = BusRepository();
	
	final _school = BehaviorSubject<SchoolContent>();
	final _bussesList = BehaviorSubject<List<BusContent>>();
	final _logoutChannel = BehaviorSubject<Null>();
	
	HomeBloc() {
		_school.listen((_) => findSchoolBus());
	}
	
	Stream<String> get schoolName => _school.stream.transform(StreamTransformer<SchoolContent, String>.fromHandlers(handleData: (val, sink) => sink.add(val.NamaSekolah)));
	Function(SchoolContent) get setSchool => _school.add;
	
	Stream<List<BusContent>> get bussesDetail => _bussesList.stream;
	Stream<Null> get logoutChannel => _logoutChannel.stream;
	
	Future<Null> findSchoolBus() async {
		final undecoded = await storage.read(key: "credential");
		if (undecoded == null) { return _logoutChannel.add(null); }
		
		var busModel = await _repository.list();
		final schoolName = _school.value == null? "": _school.value.NamaSekolah.toUpperCase();
		_bussesList.add(busModel.where((lo1) => lo1.company.contains(schoolName)).toList());
	}

	dispose() {
		_bussesList.close();
		_logoutChannel.close();
		_school.close();
	}
}

