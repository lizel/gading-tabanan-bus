import 'package:flutter/material.dart';
import 'package:tabanan/src/data/user/blocs/register_bloc.dart';
import 'package:tabanan/src/components/common_dialog.dart';
import 'package:tabanan/src/data/common/models/string_model.dart';
import 'package:tabanan/src/routes/school_search_route.dart';
import 'package:tabanan/src/data/school/models/index.dart';

class RegisterPanelBodyFormState extends State<RegisterPanelBodyForm>  {
	final bloc = RegisterBloc();
	
	@override
	void initState() {
		super.initState();
		bloc.registration.listen(_showRegistrationBuild);
	}
	
	_submit() {
		CommonDialog.showLoadingDialog(context);
		bloc.submit().whenComplete(() => Navigator.pop(context));
	}
	
	_showRegistrationBuild(StringModel snapshot) {
		
		
		return CommonDialog.showAlert(context, (context) {
			return AlertDialog(
				title: Text("Registrasi"),
				content: Column(
					mainAxisSize: MainAxisSize.min,
					crossAxisAlignment: CrossAxisAlignment.stretch,
					children: <Widget>[
						
						Text(snapshot.message),
						
						SizedBox(height: 20),
						
						SizedBox(
							height: 40,
							child: RaisedButton(
								onPressed: () => snapshot.value == 0? Navigator.pop(context): Navigator.popUntil(context, (route) => route.settings.name == "/user/login"),
								child: Text("Ok"),
							),
						)
					
					],
				),
			);
		});
	}
	
	@override
	Widget build(BuildContext context) {
		return Column(
			crossAxisAlignment: CrossAxisAlignment.stretch,
			mainAxisAlignment: MainAxisAlignment.start,
			mainAxisSize: MainAxisSize.min,
			children: <Widget>[
				
				StreamBuilder(
					stream: bloc.username,
					builder: (context, snapshot) {
						return TextField(
							onChanged: bloc.setUsername,
							decoration: InputDecoration(
								labelText: "User ID",
								hintText: "Input User ID terdaftar",
								hintStyle: Theme.of(context).textTheme.caption.copyWith(),
								suffixIcon: Icon(IconData(0xf345, fontFamily: "ionicons")),
								errorText: snapshot.error,
								errorMaxLines: 2
							),
						);
					},
				),
				
				StreamBuilder(
					stream: bloc.email,
					builder: (context, snapshot) {
						return TextField(
							onChanged: bloc.setEmail,
							keyboardType: TextInputType.emailAddress,
							decoration: InputDecoration(
								labelText: "E-Mail",
								hintText: "Input E-Mail.",
								hintStyle: Theme.of(context).textTheme.caption.copyWith(),
								suffixIcon: Icon(IconData(0xF1B8, fontFamily: "ionicons")),
								errorText: snapshot.error,
								errorMaxLines: 2,
							),
						);
					},
				),
				
				StreamBuilder(
					stream: bloc.password,
					builder: (context, snapshot) {
						return TextField(
							onChanged: bloc.setPassword,
							decoration: InputDecoration(
								labelText: "Kata Sandi",
								hintText: "Input Kata Sandi.",
								hintStyle: Theme.of(context).textTheme.caption.copyWith(),
								hintMaxLines: 3,
								suffixIcon: Icon(IconData(0xf1b0, fontFamily: "ionicons")),
								errorText: snapshot.error,
								errorMaxLines: 2,
							),
							obscureText: true,
						);
					},
				),
				
				SizedBox(height: 20),
				
				SizedBox(
					height: 40,
					child: StreamBuilder(
						stream: bloc.validForm,
						builder: (context, snapshot) {
							return RaisedButton (
								onPressed: snapshot.hasData? _submit: null,
								child: Text("DAFTAR"),
								shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
							);
						},
					),
				)
			],
		);
	}
	
	@override
	void dispose() {
		bloc.dispose();
		super.dispose();
	}
}

class RegisterPanelBodyForm extends StatefulWidget {
	RegisterPanelBodyFormState createState() => RegisterPanelBodyFormState();
}



