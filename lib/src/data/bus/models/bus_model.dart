import 'package:json_annotation/json_annotation.dart';

part 'bus_model.g.dart';

@JsonSerializable()
class BusModel {
	List<BusContent> message;
	
	BusModel(this.message);
	
	factory BusModel.fromJson(Map<String, dynamic> json) => _$BusModelFromJson(json);
	Map<String, dynamic> toJson() => _$BusModelToJson(this);
}

@JsonSerializable()
class BusContent {
	int vehicle_id;
	String license_plate_no;
	DateTime date_time;
	double lat;
	double lng;
	int km;
	String merk;
	String type;
	String halo;
	String company;
	bool status_engine;
	
	BusContent(this.vehicle_id, this.license_plate_no, this.date_time, this.lat, this.lng, this.km, this.merk, this.type, this.halo, this.company);
	
	factory BusContent.fromJson(Map<String, dynamic> json) => _$BusContentFromJson(json);
	Map<String, dynamic> toJson() => _$BusContentToJson(this);
}