import 'package:meta/meta.dart';
import '../common/user_provider_tiers.dart';
import 'package:tabanan/src/data/common/models/string_model.dart';
import '../forms/index.dart';
import '../models/index.dart';
import '../providers/user_api_provider.dart';

class UserRepository {
	final List<UserSource> sources = [ UserApiProvider() ];
	
	Future<StringModel> login({ @required LoginForm form }) {
		return sources[0].login(form: form);
	}
	
	Future<StringModel> register({ @required RegisterForm form }) {
		return sources[0].register(form: form);
	}
	
	Future<StringModel> edit({ @required RegisterForm form }) {
		return sources[0].edit(form: form);
	}
	
	Future<StringModel> reset({ @required String username }) {
		return sources[0].reset(username: username);
	}
	
	Future<DetailModel> detail({ @required String username }) {
		return sources[0].detail(username: username);
	}

}