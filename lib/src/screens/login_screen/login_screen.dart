import 'package:flutter/material.dart';
import 'component/login_panel_body.dart';
import 'component/login_panel_header.dart';
import 'component/login_panel_footer_.dart';

class LoginScreen extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			body: LayoutBuilder(
				builder: (BuildContext context, BoxConstraints viewportConstraints) {
					return SingleChildScrollView(
						child: ConstrainedBox(
							constraints: BoxConstraints(minHeight: viewportConstraints.maxHeight),
							child: Container(
								padding: EdgeInsets.symmetric(horizontal: 20),
								child: Column(
									crossAxisAlignment: CrossAxisAlignment.stretch,
									mainAxisAlignment: MainAxisAlignment.spaceBetween,
									mainAxisSize: MainAxisSize.max,
									children: <Widget>[
										LoginPanelHeader(),
										
										Column(
											crossAxisAlignment: CrossAxisAlignment.stretch,
											mainAxisAlignment: MainAxisAlignment.start,
											mainAxisSize: MainAxisSize.min,
											children: <Widget>[
												LoginPanelBody(),
												LoginPanelFooter()
											],
										)
									],
								),
							),
						),
					);
				}
			)
		);
	}
}