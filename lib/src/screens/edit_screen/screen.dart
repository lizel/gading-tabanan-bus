import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'form.dart';
import 'package:tabanan/src/routes/home_route.dart';

class Screen extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				leading: IconButton(
					icon: Icon(Icons.close),
					onPressed: () => Navigator.pushAndRemoveUntil(context, HomeRoute.build(), (route) => route.settings.name == "/user/home")
				),
				automaticallyImplyLeading: false,
				brightness: Brightness.light,
				elevation: 3,
				iconTheme: Theme.of(context).iconTheme.copyWith(color: Colors.black),
				backgroundColor: Colors.white,
				title: Text("Akun Saya"),
				textTheme: Theme.of(context).textTheme,
			),
			body: LayoutBuilder(
				builder: (BuildContext context, BoxConstraints viewportConstraints) {
					return SingleChildScrollView(
						child: ConstrainedBox(
							constraints: BoxConstraints(minHeight: viewportConstraints.maxHeight),
							child: Container(
								padding: EdgeInsets.symmetric(horizontal: 20),
								child: EditForm()
							),
						),
					);
				}
			)
		);
	}
}

