import 'package:flutter/material.dart';
import 'package:tabanan/src/components/state_data_widget.dart';
import 'package:tabanan/src/data/school/blocs/search_bloc.dart';
import 'component/search_bar.dart';
import 'component/body.dart';

class ScreenState extends State<Screen> {
	final bloc = SearchBloc();
	
	@override
	void initState() {
		super.initState();
		bloc.searchFor("");
	}
	
	@override
	Widget build(BuildContext context) {
		return StateDataWidget<ScreenState>(
			state: this,
			child: Scaffold(
				body: LayoutBuilder(
					builder: (BuildContext context, BoxConstraints viewportConstraints) {
						return ConstrainedBox(
							
							constraints: BoxConstraints(
								maxHeight: viewportConstraints.maxHeight,
								maxWidth: viewportConstraints.maxWidth
							),
							
							child: Column(
								crossAxisAlignment: CrossAxisAlignment.stretch,
								mainAxisAlignment: MainAxisAlignment.start,
								mainAxisSize: MainAxisSize.max,
								children: <Widget>[ SearchBar(), Flexible(child: Body()) ],
							),
							
						);
					}
				),
			),
		);
	}
	
	
	@override
	  void dispose() {
		bloc.dispose();
		super.dispose();
	  }
}



class Screen extends StatefulWidget {
	@override
  	State<StatefulWidget> createState() => ScreenState();
}

