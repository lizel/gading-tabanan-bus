import 'package:tabanan/src/data/common/models/string_model.dart';
import 'package:tabanan/src/data/user/models/index.dart';
import '../forms/register_form.dart';
import '../forms/login_form.dart';

abstract class UserSource {
	Future<StringModel> login({ LoginForm form });
	Future<StringModel> register({ RegisterForm form });
	Future<StringModel> edit({ RegisterForm form });
	Future<StringModel> reset({ String username });
	Future<DetailModel> detail({ String username });
}