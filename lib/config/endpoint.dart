import 'package:json_annotation/json_annotation.dart';

part 'endpoint.g.dart';

@JsonSerializable()
class EndpointConfiguration {
	String url;
	String domain;
	
	
	EndpointConfiguration(this.url, this.domain);
	
	/*
	 * JSON Function
	 */
	factory EndpointConfiguration.fromJson(Map<String, dynamic> json) => _$EndpointConfigurationFromJson(json);
	Map<String, dynamic> toJson() => _$EndpointConfigurationToJson(this);
}

@JsonLiteral("endpoint.json", asConst: true)
Map<String, dynamic> get endpointMaps => _$endpointMapsJsonLiteral;
final EndpointConfiguration endpointConfiguration = EndpointConfiguration.fromJson(endpointMaps);