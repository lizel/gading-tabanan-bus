// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bus_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BusModel _$BusModelFromJson(Map<String, dynamic> json) {
  return BusModel((json['message'] as List)
      ?.map((e) =>
          e == null ? null : BusContent.fromJson(e as Map<String, dynamic>))
      ?.toList());
}

Map<String, dynamic> _$BusModelToJson(BusModel instance) =>
    <String, dynamic>{'message': instance.message};

BusContent _$BusContentFromJson(Map<String, dynamic> json) {
  return BusContent(
      json['vehicle_id'] as int,
      json['license_plate_no'] as String,
      json['date_time'] == null
          ? null
          : DateTime.parse(json['date_time'] as String),
      (json['lat'] as num)?.toDouble(),
      (json['lng'] as num)?.toDouble(),
      json['km'] as int,
      json['merk'] as String,
      json['type'] as String,
      json['halo'] as String,
      json['company'] as String)
    ..status_engine = json.containsKey('status_engine')? json['status_engine'] == "true": false;
}

Map<String, dynamic> _$BusContentToJson(BusContent instance) =>
    <String, dynamic>{
      'vehicle_id': instance.vehicle_id,
      'license_plate_no': instance.license_plate_no,
      'date_time': instance.date_time?.toIso8601String(),
      'lat': instance.lat,
      'lng': instance.lng,
      'km': instance.km,
      'merk': instance.merk,
      'type': instance.type,
      'halo': instance.halo,
      'company': instance.company,
      'status_engine': instance.status_engine
    };
