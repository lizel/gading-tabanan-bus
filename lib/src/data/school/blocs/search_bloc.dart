import 'dart:async';
import 'package:rxdart/rxdart.dart';
import '../models/index.dart';
import '../repositories/index.dart';

class SearchBloc {
	final _repository = SchoolRepository();
	final _search = BehaviorSubject<String>();
	final _schools = BehaviorSubject<List<SchoolContent>>();
	
	SearchBloc() {
		_search
			.stream
			.bufferTime(Duration(milliseconds: 1000))
			.listen(_queryRepository);
	}
	
	_queryRepository(List<String> keywords) async {
		if (keywords.length == 0) { return; }
		final result = await _repository.list();
		final filtered = result
			.message
			.where((val) => val.NamaSekolah.toLowerCase().contains(keywords.last) || val.NamaSekolah.toUpperCase().contains(keywords.last)).toList();
		_schools.sink.add(filtered);
	}
	
	Function(String) get searchFor => _search.sink.add;
	Stream<List<SchoolContent>> get check => _schools.stream;

	dispose() {
		_search.close();
		_schools.close();
	}
}

