import 'package:flutter/material.dart';
import '../../../components/logo_circle_view.dart';

class RegisterPanelHeader extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Container(
			height: 280,
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.stretch,
				mainAxisAlignment: MainAxisAlignment.center,
				mainAxisSize: MainAxisSize.max,
				children: <Widget>[
					Container(
						width: 200,
						height: 200,
						child: LogoCircleView(),
					),
					
				],
			)
		);
	}
}