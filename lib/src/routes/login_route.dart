import 'package:flutter/material.dart';
import 'common_route.dart';
import '../screens/login_screen/login_screen.dart';

class LoginRoute extends CommonRoute {
	
	LoginRoute({@required WidgetBuilder builder, RouteSettings settings, bool maintainState: false, bool fullscreenDialog: false }):
			super(builder: builder, settings: settings, maintainState: maintainState, fullscreenDialog: fullscreenDialog);
	
	
	factory LoginRoute.build({ RouteSettings settings, bool maintainState: true, bool fullscreenDialog: false }) {
		return LoginRoute(
			builder: (BuildContext context) { return LoginScreen(); },
			settings: RouteSettings(name: "/user/login"),
			maintainState: maintainState,
			fullscreenDialog: fullscreenDialog
		);
	}
	
	@override
	Widget buildTransitions(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
		return SlideTransition(
			position: new Tween<Offset>(
				begin: Offset(0.0, 1.0),
				end: Offset.zero,
			).animate(animation),
			child: child,
		);

//		return FadeTransition(
//			opacity: Tween<double>(begin: 0, end: 1).animate(secondaryAnimation),
//			child: FadeTransition(
//				opacity: Tween<double>(begin: 1, end: 0).animate(animation),
//				child: child,
//			)
//		);
	}
}