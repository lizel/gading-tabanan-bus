import 'package:flutter/material.dart';

class LogoCircleView extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Container(
			child: Container(
				child: ClipOval(child: Image.asset('assets/images/app/logo.png')),
				decoration: BoxDecoration(
					shape: BoxShape.circle,
					border: Border.all(color: Colors.red[100], width: 10)
				),
			),
			decoration: BoxDecoration(
				shape: BoxShape.circle,
					border: Border.all(color: Colors.red[50], width: 15)
			),
		);
	}
}