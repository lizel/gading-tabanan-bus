import 'package:flutter/material.dart';
import 'common_route.dart';
import '../screens/authentication/screen.dart';

class AuthenticationRoute extends CommonRoute {
	
	AuthenticationRoute({@required WidgetBuilder builder, RouteSettings settings, bool maintainState: false, bool fullscreenDialog: false }):
			super(builder: builder, settings: settings, maintainState: maintainState, fullscreenDialog: fullscreenDialog);
	
	
	factory AuthenticationRoute.build({ RouteSettings settings, bool maintainState: true, bool fullscreenDialog: false }) {
		return AuthenticationRoute(
			builder: (BuildContext context) { return AuthenticationScreen(); },
			settings: RouteSettings(name: "/user/auth"),
			maintainState: maintainState,
			fullscreenDialog: fullscreenDialog
		);
	}
	
	@override
	Widget buildTransitions(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
//		return SlideTransition(
//			position: new Tween<Offset>(
//				begin: Offset(0.0, 1.0),
//				end: Offset.zero,
//			).animate(animation),
//			child: child,
//		);

		return FadeTransition(
			opacity: Tween<double>(begin: 0, end: 1).animate(animation),
			child: child
		);
	}
}