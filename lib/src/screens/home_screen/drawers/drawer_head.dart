import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class DrawerHead extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Container (
			padding: EdgeInsets.symmetric(horizontal: 10),
			height: 70,
			child: Row (
				crossAxisAlignment: CrossAxisAlignment.center,
				mainAxisAlignment: MainAxisAlignment.start,
				mainAxisSize: MainAxisSize.max,
				children: <Widget>[
					
					Image.asset(
						'assets/images/app/logo.png',
						width: 50,
						height: 50,
						fit: BoxFit.contain,
					),
					
					SizedBox(width: 10),
					
					Image.asset(
						'assets/images/app/tabanan-logo.png',
						width: 50,
						height: 50,
						fit: BoxFit.contain,
					),
					
					Flexible(flex: 1, child: Container()),
					
					RawMaterialButton(
						padding: EdgeInsets.all(5),
						child: Icon(Icons.close),
						shape: new CircleBorder(),
						onPressed: () => Navigator.pop(context),
						constraints: BoxConstraints(
							maxHeight: 40,
							maxWidth: 40
						),
					)
				
				],
			),
		);
	}
}