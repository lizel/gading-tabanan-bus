import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:tabanan/src/data/bus/blocs/home_bloc.dart';
import 'package:tabanan/src/data/bus/models/index.dart';
import 'package:tabanan/src/routes/driver_route.dart';

//	static final CameraPosition _kLake = CameraPosition(
//		bearing: 192.8334901395799,
//		target: LatLng(37.43296265331129, -122.08832357078792),
//		tilt: 59.440717697143555,
//		zoom: 19.151926040649414);

class MapCanvasState extends State<MapCanvas> {
	final _defaultCameraPosition = CameraPosition(
		target: LatLng(-8.340539, 115.091949),
		zoom: 11,
	);
	
	final Set<Marker> _markers = {};
	final Completer<GoogleMapController> _controller = Completer();
	final HomeBloc bloc;
	
	MapCanvasState(this.bloc);
	
	@override
	void initState() {
		super.initState();
		bloc.bussesDetail.listen((vals) {
			setState(() {
				_controller.future.then((ctrl) {
					_markers.clear();
					vals.forEach(_drawBus);
					if (vals.length > 0) {
						ctrl.animateCamera(CameraUpdate.newLatLngZoom(LatLng(vals[0].lat, vals[0].lng), 10));
					}
				});
			});
		});
		
		bloc.findSchoolBus();
	}
	
	_drawBus(BusContent aBus) {
		final coor = LatLng(aBus.lat, aBus.lng);
		_markers.add(Marker(
			markerId: MarkerId(aBus.license_plate_no),
			position: coor,
			infoWindow: InfoWindow(
				title: aBus.company,
				snippet: aBus.license_plate_no
			),
			icon: aBus.status_engine? BitmapDescriptor.fromAsset("assets/images/app/bus_marker_icon.png"): BitmapDescriptor.fromAsset("assets/images/app/bus_imarker_icon.png"),
			consumeTapEvents: false,
			onTap: () => Navigator.push(context, DriverRoute.build(platNo: aBus.license_plate_no))
		));
	}
	
	@override
	Widget build(BuildContext context) {
		return GoogleMap(
			mapType: MapType.normal,
			initialCameraPosition: _defaultCameraPosition,
			onMapCreated: _controller.complete,
			markers: _markers,
		);
	}
}

class MapCanvas extends StatefulWidget {
	final HomeBloc bloc;
	
	MapCanvas(this.bloc);
	
	@override
  	State<StatefulWidget> createState() => MapCanvasState(bloc);
}